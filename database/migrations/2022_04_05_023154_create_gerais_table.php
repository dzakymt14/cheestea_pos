<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gerais', function (Blueprint $table) {
            $table->id();
            $table->string('nama_gerai');
            $table->string('lokasi_gerai')->nullable();
            $table->string('lat_lng_gerai')->nullable();
            $table->string('kode_gerai')->nullable();
            $table->string('pin_gerai')->nullable();
            $table->string('status_gerai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gerais');
    }
}
