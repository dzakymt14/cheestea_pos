<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->id();
            $table->integer('id_kategori');
            $table->string('tipe_inventory');
            $table->string('brand_inventory')->nullable();
            $table->string('satuan_inventory')->nullable();
            $table->string('kuantitas_inventory')->nullable();
            $table->string('harga_inventory')->nullable();
            $table->string('berkas_inventory')->nullable();
            $table->string('status_inventory')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
