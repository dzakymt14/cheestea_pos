<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
  protected $fillable = [
    'id_kategori',
    'tipe_inventory',
    'brand_inventory',
    'satuan_inventory',
    'kuantitas_inventory',
    'harga_inventory',
    'berkas_inventory',
    'status_inventory',
  ];
}
