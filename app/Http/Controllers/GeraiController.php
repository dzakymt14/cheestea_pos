<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Gerai;
use App\UserGerai;
use DB;

class GeraiController extends Controller
{
    public function geraiIndex($status){
      $gerais = Gerai::where('status_gerai',$status)->get();
      $status = $status;
      // dd($gerais);
      return view('gerai.index',compact('gerais','status'));
    }

    public function geraiPost(Request $request){
      $validator = Validator::make($request->all(),[
          'nama_gerai' => 'required|max:255',
          'master_pin' => 'required|max:6',
          're_master_pin' => 'required|max:6',
          'alamat' => 'required',
        ]);
      if ($validator->fails()) {
          return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        if ($request->master_pin == $request->re_master_pin) {
          $gerai = Gerai::create([
            'nama_gerai' => $request->nama_gerai,
            'lokasi_gerai' => $request->alamat,
            'lat_lng_gerai' => $request->lat .'/'.$request->lng,
            'status_gerai'  => 'aktif',
            'pin_gerai'     => $request->master_pin,
          ]);



          if($request->id_user != null){
            for ($i=0; $i < count($request->id_user); $i++) {
              UserGerai::create([
                'id_user' => $request->id_user[$i],
                'id_gerai' => $gerai->id,
                'pin_user_gerai' => rand(100000, 999999),
                'status_user_gerai' => 'aktif'
              ]);
            }
          }
          return redirect()->back()->with('toast_success', 'Gerai Berhasil Di Buat');
        }else{
          return redirect()->back()->with('toast_error', 'Pin Yang anda Isi Berbeda');
        }
      }
    }

    public function geraiAktifasi($code){
      $tmp = explode('||',$code);
      $gerai = Gerai::find($tmp[0]);
      if ($tmp[1]==0) {
        $gerai->update([
          'status_gerai'=>'nonaktif'
        ]);
      }elseif ($tmp[1]==1) {
        $gerai->update([
          'status_gerai'=>'aktif'
        ]);
      }
      return redirect()->back()->with('toast_success', 'Berhasil dilakukan');
    }

    public function geraiDetail(){
      return view('gerai.detail');
    }
}
