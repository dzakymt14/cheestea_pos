<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriInventory;
use App\inventory;
use DB;

class InventoryController extends Controller
{
    public function inventoryIndex(){
      $kategories = KategoriInventory::all();
      return view('inventory.index',compact('kategories'));
    }

    public function inventoryPost(Request $request){
      $kategori = KategoriInventory::where('nama_kategori',strtolower($request->nama_kategori))->first();
      if ($kategori == false) {
        $kategori = KategoriInventory::create([
          'nama_kategori'=>strtolower($request->nama_kategori),
        ]);
      }

      $total_kuantitas = $request->kelipatan_item * $request->kuantitas_inventory;
      inventory::create([
        'id_kategori'=>$kategori->id,
        'tipe_inventory'=>$request->tipe_inventory,
        'brand_inventory'=>$request->brand_inventory,
        'satuan_inventory'=>$request->satuan_inventory,
        'kuantitas_inventory'=>$total_kuantitas,
        'harga_inventory'=>$request->harga_inventory,
        'berkas_inventory'=>$request->berkas_inventory,
        'status_inventory'=>$request->status_inventory,
        'status_inventory'=>'instock'
      ]);
        return redirect()->back()->with('toast_success', 'data tersimpan');
    }
}
