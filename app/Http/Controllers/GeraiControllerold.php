<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\ValidationException;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use App\Gerai;
use App\User;
use App\Karyawan;
use DB;

class GeraiController extends Controller
{
    public function geraiIndex(){
        $data  = [];
        $gerai = Gerai::where('status_gerai', 1)->get();
        foreach ($gerai as $key => $gerais) {
          //lat lng
          $lat = explode("/",$gerais->lat_lng_gerai)[0];
          $lng = explode("/",$gerais->lat_lng_gerai)[1];

          //status
          $status = '';
          $badge = '';
          if ($gerais->status_gerai == 1) {
            $status = 'aktif';
            $badge = 'badge-light-success';
          }elseif ($gerais->status_gerai == 0){
            $status = 'Non Aktif';
            $badge = 'badge-light-danger';
          }
          $karyawan = [];
          $data[$key] = [
            'id' => $gerais->id,
            'name' => $gerais->nama_gerai,
            'alamat' => $gerais->lokasi_gerai,
            'lat' => $lat,
            'lng' => $lng,
            'pin' => $gerais->pin_gerai,
            'status_val' => $gerais->status_gerai,
            'status_desc' => $status,
            'status_badge' => $badge,
          ];
        }
      return view('gerai.index',compact('data'));
    }
    public function geraiIndexNonAktif(){
      $data  = [];
      $gerai = Gerai::where('status_gerai', 0)->get();
      foreach ($gerai as $key => $gerais) {
        //lat lng
        $lat = explode("/",$gerais->lat_lng_gerai)[0];
        $lng = explode("/",$gerais->lat_lng_gerai)[1];

        //status
        $status = '';
        $badge = '';
        if ($gerais->status_gerai == 1) {
          $status = 'aktif';
          $badge = 'badge-light-success';
        }elseif ($gerais->status_gerai == 0){
          $status = 'Non Aktif';
          $badge = 'badge-light-danger';
        }
        $karyawan = [];
        $data[$key] = [
          'id' => $gerais->id,
          'name' => $gerais->nama_gerai,
          'alamat' => $gerais->lokasi_gerai,
          'lat' => $lat,
          'lng' => $lng,
          'pin' => $gerais->pin_gerai,
          'status_val' => $gerais->status_gerai,
          'status_desc' => $status,
          'status_badge' => $badge,
        ];
      }
      return view('gerai.index',compact('data'));
    }
    public function create(Request $request)
    {

      $validator = Validator::make($request->all(),[
          'nama_gerai' => 'required|max:255',
          'master_pin' => 'required|max:6',
          're_master_pin' => 'required|max:6',
          'alamat' => 'required',
        ]);
      if ($validator->fails()) {
          return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        if ($request->master_pin == $request->re_master_pin) {
          $checkLastCode = Gerai::orderBy('id', 'desc')->first();
          $kode_gerai = 0;
          if($checkLastCode == null){
            $kode_gerai = 1;
          }else{
            $kode_gerai = $checkLastCode->id + 1;
          }
          $gerai = Gerai::create([
            'nama_gerai' => $request->nama_gerai,
            'lokasi_gerai' => $request->alamat,
            'lat_lng_gerai' => $request->lat .'/'.$request->lng,
            'status_gerai'  => 0,
            'kode_gerai'    => $kode_gerai,
            'pin_gerai'     => $request->master_pin,
          ]);

          $user_id = $request->user_id;
          if($user_id != null){
            for ($i=0; $i < count($user_id); $i++) {
              $karyawan = Karyawan::create([
                'gerai_id' => $gerai->id,
                'user_id' => $user_id[$i],
                'pin' => rand(100000, 999999),
              ]);
            }
          }
          return redirect()->back()->with('toast_success', 'Gerai Berhasil Di Buat');
        }else{
          return redirect()->back()->with('toast_error', 'Pin Yang anda Isi Berbeda');
        }
      }
    }
    public function geraiDetail($id){
      try {
        $user = Gerai::findOrFail($id);
      } catch (ModelNotFoundException $exception) {
        return back()->withError($exception->getMessage())->withInput();
      }
      return view('gerai.detail');
    }
}
