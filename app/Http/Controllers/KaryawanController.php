<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\File;


class KaryawanController extends Controller
{
    public function karyawanIndex(){
      $karyawans = User::where('level','karyawan')->get();
      return view('karyawan.index',compact('karyawans'));
    }

    public function karyawanPost(Request $request){
      //jika  create
      if ($request->code_post == 1) {
        $path = public_path('berkas/karyawan/');
        $foto_user = 'blank.png';
        if (!File::isDirectory($path)) {
          File::makeDirectory($path, 0777, true, true);
        }
        if ($request->foto_user != null && $request->foto_user != '') {
          $foto_user = uniqid('img_') . '.' . $request->foto_user->getClientOriginalExtension();
          $request->foto_user->move($path, $foto_user);
        }


        User::create([
          'level'=>'karyawan',
          'nama'=>$request->nama,
          'email'=>$request->email,
          'no_tlpn'=>$request->no_tlpn,
          'password'   => bcrypt('cheestea'),
          'status_user'=>'aktif',
          'foto_user'=>$foto_user
        ]);
        //jika edit
      }elseif($request->code_post == 2){
        $user = User::find($request->id_user);
        $path = public_path('berkas/karyawan/');
        $foto_user = 'blank.png';
        if (!File::isDirectory($path)) {
          File::makeDirectory($path, 0777, true, true);
        }
        if ($request->judul_foto_user != $user->foto_user && $request->foto_user != null && $request->foto_user != '') {
          $foto_user = uniqid('img_') . '.' . $request->foto_user->getClientOriginalExtension();
          $request->foto_user->move($path, $foto_user);
        }elseif($request->judul_foto_user == $user->foto_user && $request->foto_user == null) {
          $foto_user = $user->foto_user;
        }
        $user->update([
          'nama'=>$request->nama,
          'email'=>$request->email,
          'no_tlpn'=>$request->no_tlpn,
          'password'=>bcrypt('cheestea'),
          'foto_user'=>$foto_user,
        ]);

        //jika non aktif
      }else {
        $user = User::find($request->id_user);
        $user->update([
          'status_user'=>'tidak aktif'
        ]);
      }

      return redirect()->back()->with('toast_success', 'data tersimpan');
    }
}
