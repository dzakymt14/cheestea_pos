<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class KasirController extends Controller
{
    public function kasir(){
      $minuman = Menu::where('tipe_menu','minum')->get();
      $makanan = Menu::where('tipe_menu','makan')->get();
      $add = Menu::where('tipe_menu','add')->get();
      $data = [
        'minuman'=>$minuman,
        'makanan'=>$makanan,
        'add'=>$add,
      ];
      return view('kasir.index',compact('data'));
    }
}
