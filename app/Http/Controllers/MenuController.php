<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Menu;
use App\komposisi;
use DB;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{
    public function index(){
      $menus = Menu::all();
      return view('menu.index',compact('menus'));
    }
    public function menuPost(Request $request)
    {
      $imageName = 'blank.png';
        if ($request->avatar != null && $request->avatar != '') {
            $imageName = uniqid('img_menu_').'.'.$request->avatar->getClientOriginalExtension();
            $path = public_path('berkas/menu');
            $gambar = Image::make($request->avatar)->resize(200,200);
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
                $gambar->save(public_path('berkas/menu/'.$imageName),80);
            }else{
                $gambar->save(public_path('berkas/menu/'.$imageName),80);
            }
        }
        $create = Menu::create([
          'nama_menu'  => $request->nama_menu,
          'harga_menu' => $request->harga_menu,
          'tipe_menu' =>  $request->tipe_menu,
          'foto_menu' =>  $imageName,
          'status_menu' => 1,
        ]);

        if (count($request->kategori_komposisi)>0) {
          for ($i=0; $i < count($request->kategori_komposisi); $i++) {
            $komposisi = komposisi::create([
              'menu_id'  => $create->id,
              'kategori_komposisi' => $request->kategori_komposisi[$i],
              'jumlah_komposisi'   => $request->jumlah_komposisi[$i],
              'satuan_komposisi'   => $request->satuan_komposisi[$i],
            ]);
          }
        }

        return redirect()->back()->with('toast_success', 'Menu Berhasil Di Buat');
    }
}
