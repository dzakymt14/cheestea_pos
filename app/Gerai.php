<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gerai extends Model
{
    protected $fillable = [
      'nama_gerai',
      'lokasi_gerai',
      'lat_lng_gerai',
      'status_gerai',
      'kode_gerai',
      'pin_gerai'
  ];
}
