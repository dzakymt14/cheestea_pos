<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriInventory extends Model
{
  protected $fillable = [
      'nama_kategori'
  ];
}
