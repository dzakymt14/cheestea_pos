<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
  protected $fillable = [
    'nama_menu',
    'harga_menu',
    'tipe_menu',
    'foto_menu',
    'status_menu',
  ];
}
