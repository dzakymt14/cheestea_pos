<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komposisi extends Model
{
  protected $fillable = [
    'menu_id',
    'kategori_komposisi',
    'jumlah_komposisi',
    'satuan_komposisi',
  ];
}
