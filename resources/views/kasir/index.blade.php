@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
					<div class="col-xl-12">
						<div class="row gy-0 gx-10">
							<div class="col-12">
								<div class="row">
									<div class="col-xl-8 col-6">
										<ul class="nav row mb-8">
											<li class="nav-item col-4 mb-5 mb-lg-0">
												<a class="nav-link btn btn-flex btn-color-muted btn-outline btn-outline-default btn-active-dark d-flex flex-grow-1 flex-column flex-center py-8 active" data-bs-toggle="tab" href="#kt_general_widget_1_1">
													<span class="svg-icon svg-icon-3x mb-5 mx-0">
														<i class="fas fa-coffee fs-1"></i>
													</span>
													<span class="fs-6 fw-bold">Minuman</span>
												</a>
											</li>
											<li class="nav-item col-4 mb-5 mb-lg-0">
												<a class="nav-link btn btn-flex btn-color-muted btn-outline btn-outline-default btn-active-dark d-flex flex-grow-1 flex-column flex-center py-8" data-bs-toggle="tab" href="#kt_general_widget_1_2">
													<span class="svg-icon svg-icon-3x mb-5 mx-0">
														<i class="fas fa-hamburger fs-1"></i>
													</span>
													<span class="fs-6 fw-bold">Makanan</span>
												</a>
											</li>
											<li class="nav-item col-4 mb-5 mb-lg-0">
												<a class="nav-link btn btn-flex btn-color-muted btn-outline btn-outline-default btn-active-dark d-flex flex-grow-1 flex-column flex-center py-8" data-bs-toggle="tab" href="#kt_general_widget_1_3">
													<span class="svg-icon svg-icon-3x mb-5 mx-0">
														<i class="fas fa-utensils fs-1"></i>
													</span>
													<span class="fs-6 fw-bold">Add On</span>
												</a>
											</li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane fade active show" id="kt_general_widget_1_1">
												<div class="row">
													<div class="col-12 mb-3">
														<h5>Total : {{$data['minuman']->count()}}</h5>
													</div>
													@foreach ($data['minuman'] as $key => $menu)
														<div class="col-xl-3 col-md-4 mb-5">
															<a href="#!" class="menuButton" id="{{$menu->id}}||{{$menu->nama_menu}}||{{$menu->harga_menu}}">
																<div class="card-xl-stretch card-rounded border">
																	<div class="card-body">
																		<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{('berkas')}}/menu/{{$menu->foto_menu}}')"></div>
																		<div class="mt-5">
																			<h5 class="fw-bolder text-dark mb-0">{{$menu->nama_menu}}</h5>
																			<span class="fs-6 fw-bolder text-gray-600">
																				<span class="fs-7 fw-bold text-gray-600">Rp</span> {{number_format($menu->harga_menu,0)}}</span>
																			</div>
																		</div>
																	</div>
																</a>
															</div>
													@endforeach
													</div>
												</div>
												<div class="tab-pane fade" id="kt_general_widget_1_2">
													<div class="row">
														<div class="col-12 mb-3">
															<h5>Total : {{$data['makanan']->count()}}</h5>
														</div>
														@foreach ($data['makanan'] as $key => $menu)
															<div class="col-xl-3 col-md-4 mb-5">
																<a href="#!" class="menuButton" id="{{$menu->id}}||{{$menu->nama_menu}}||{{$menu->harga_menu}}">
																	<div class="card-xl-stretch card-rounded border">
																		<div class="card-body">
																			<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{('berkas')}}/menu/{{$menu->foto_menu}}')"></div>
																			<div class="mt-5">
																				<h5 class="fw-bolder text-dark mb-0">{{$menu->nama_menu}}</h5>
																				<span class="fs-6 fw-bolder text-gray-600">
																					<span class="fs-7 fw-bold text-gray-600">Rp</span> {{number_format($menu->harga_menu,0)}}</span>
																				</div>
																			</div>
																		</div>
																	</a>
																</div>
														@endforeach
														</div>
													</div>
													<div class="tab-pane fade" id="kt_general_widget_1_3">
														<div class="row">
															<div class="col-12 mb-3">
																<h5>Total : {{$data['add']->count()}}</h5>
															</div>
															@foreach ($data['add'] as $key => $menu)
																<div class="col-xl-3 col-md-4 mb-5">
																	<a href="#!" class="menuButton" id="{{$menu->id}}||{{$menu->nama_menu}}||{{$menu->harga_menu}}">
																		<div class="card-xl-stretch card-rounded border">
																			<div class="card-body">
																				<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{('berkas')}}/menu/{{$menu->foto_menu}}')"></div>
																				<div class="mt-5">
																					<h5 class="fw-bolder text-dark mb-0">{{$menu->nama_menu}}</h5>
																					<span class="fs-6 fw-bolder text-gray-600">
																						<span class="fs-7 fw-bold text-gray-600">Rp</span> {{number_format($menu->harga_menu,0)}}</span>
																					</div>
																				</div>
																			</div>
																		</a>
																	</div>
															@endforeach
															</div>
														</div>
													</div>
												</div>
												<div class="col-xl-4 col-6" style="position:fixed; right: 0;">
													<div class="card card-flush py-4 flex-row-fluid">
														<div class="card-body py-3">
															<div class="row">
																<div class="col-12 mb-4">
																	<h2 class="fw-bolder mb-0">Order Details (<b class="text-warning">#14534</b>)</h2>
																	<i class="fas fa-calendar-alt me-2"></i><span class="text-gray-600 fs-7">Tanggal : <b class="text-dark">4-4-2022</b></span>
																</div>
																<div class="col-12">
																	<hr class="mt-2 text-gray-600">
																	<table id="kt_datatable_example_2" class="table table-row-bordered gs-7">
																		<thead>
																			<tr class="fw-bold fs-6 text-gray-800" style="display: none;">
																				<th>Produk</th>
																			</tr>
																		</thead>
																		<tbody id="checkout">



																		</tbody>
																	</table>
																</div>
																<div class="col-12 mt-4 generalBayar" style="display:none">
																	<h5 class="mb-3">Total :</h5>
																	<div class="fs-6 fw-bolder d-flex flex-stack">
																		<span class="badge badge-dark border border-dashed fs-2 fw-boldest text-warning p-3 total_harus_bayar">
																		</span>
																			<a href="#modal-bayar" data-bs-toggle="modal" class="btn btn-warning text-dark"><i class="fas fa-wallet me-2 text-dark"></i>Bayar</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						@include('kasir.modal.modal-bayar')

						<script type="text/javascript">
							$(document).ready(function() {
								$("#kt_datatable_example_2").DataTable({
									"scrollY": "170px",
									"scrollCollapse": true,
									"paging": false,
									"dom": "<'table-responsive'tr>"
								});
								$("#kt_datatable_example_3").DataTable({
									"scrollY": "200px",
									"scrollCollapse": true,
									"paging": false,
									"dom": "<'table-responsive'tr>"
								});
							});

								var checkout=[];var run = 0;
								var indexed = [];var x = 1;

								function index_chekout(tmp,code){
									if (code == 1) {
										//memasukan satuan ke main array isi pesanan
										checkout.push({id:tmp[0],nama_menu:tmp[1],harga_menu:tmp[2]});
										//end memasukan satuan ke main array isi pesanan
									}else if(code==0) {
											var start_index = tmp[3];
											checkout.splice(start_index,1);
									}

									if (checkout.length != 0) {
										//proses menghilangkan duplikasi untuk view
											var jumlah = 0;
											var id = tmp[0];var total_harga_menu = 0;var total_harus_bayar = 0;var index_chekout = 0;
											for (var i = 0; i < checkout.length; i++) {
												if (checkout[i]['id'] == tmp[0]) {
													jumlah += 1;
													nama_menu = tmp[1];
													harga_menu = tmp[2];
													total_harga_menu += Number(tmp[2]);
													index_chekout = i;
												}
											}
											if (code == 1) {
												if ( jumlah==1) {
													indexed.push({id:id,nama_menu:nama_menu,harga_menu:harga_menu,jumlah:jumlah,total_harga_menu:total_harga_menu,index_chekout:index_chekout});
												}else if (jumlah!=1) {
													for (var i = 0; i < indexed.length; i++) {
														if (indexed[i]['id']==id) {
															indexed[i]= {id:id,nama_menu:nama_menu,harga_menu:harga_menu,jumlah:jumlah,total_harga_menu:total_harga_menu,index_chekout:indexed[i]['index_chekout']}
														}
													}
												}

											}else {
												for (var i = 0; i < indexed.length; i++) {
													if (indexed[i]['id']==id) {
														if (indexed[i]['jumlah']>1) {
															indexed[i]= {id:id,nama_menu:nama_menu,harga_menu:harga_menu,jumlah:indexed[i]['jumlah']-1,total_harga_menu:indexed[i]['total_harga_menu']-indexed[i]['harga_menu'],index_chekout:indexed[i]['index_chekout']}
														}else {
															indexed.splice(i,1);
														}
													}
												}
											}
										//end proses menghilangkan duplikasi untuk view
										console.log(indexed.length);
										if (indexed.length==0) {
											checkout=[];
											$('.generalBayar').hide();
										}else {
											$('.generalBayar').show();
										}
									}else {
										indexed = [];
										total_harus_bayar = 0;
										$('.generalBayar').hide();
									}


									var isi = '';var isi_final = '';
									for (var i = indexed.length - 1; i >= 0; i--) {
										isi += '<tr>'+
											'<td>'+
												'<div class="row align-items-center">'+
													'<div class="col-xl-9 col-6">'+
														'<div class="d-flex align-items-center">'+
															'<h5 class="mb-0 fw-bolder">x '+indexed[i]['jumlah']+'</h5>'+
															'<div class="ms-5">'+
																'<h5 class="mb-0">'+indexed[i]['nama_menu']+'</h5>'+
																'<div>'+
																	'<h6 class="mb-0 text-gray-600 fw-bold">Rp. '+indexed[i]['index_chekout']+'</h6>'+
																'</div>'+
															'</div>'+
														'</div>'+
													'</div>'+
													'<div class="col-xl-3 col-6 text-center">'+
														'<a href="#!" class="menuButton" id="'+indexed[i]['id']+'||'+indexed[i]['nama_menu']+'||'+indexed[i]['harga_menu']+'"><i class="fas fa-plus text-success me-2"></i></a>'+
														'<a href="#!" class="kurangiPesanan" id="'+indexed[i]['id']+'||'+indexed[i]['nama_menu']+'||'+indexed[i]['harga_menu']+'||'+indexed[i]['index_chekout']+'"><i class="fas fa-minus text-danger"></i></a>'+
													'</div>'+
												'</div>'+
											'</td>'+
										'</tr>';
										isi_final += '<div class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">'+
												'<div class="d-flex align-items-center">'+
														'<h5 class="mb-0 fw-boldest">x '+indexed[i]['jumlah']+'</h5>'+
														'<div class="ms-6">'+
																'<span class="d-flex align-items-center fs-5 fw-bolder text-dark">'+indexed[i]['nama_menu']+'</span>'+
																'<span class="badge badge-light-primary fs-8 fw-bold">Rp. '+indexed[i]['harga_menu']+'</span>'+
														'</div>'+
												'</div>'+
												'<div class="d-flex">'+
														'<div class="text-end">'+
																'<div class="fs-5 fw-bolder text-dark">Rp. '+indexed[i]['total_harga_menu']+'</div>'+
														'</div>'+
												'</div>'+
										'</div>';
									}
									$("#checkout").html(isi);
									$("#checkoutFinal").html(isi_final);
									for (var i = 0; i < indexed.length; i++) {
										total_harus_bayar += Number(indexed[i]['total_harga_menu']);
									}
									$(".total_harus_bayar").html('<span class="fs-6 fw-bold text-white">Rp</span> '+total_harus_bayar);

									//dilempar ke form akhir modal yg akan di proses di kontroller
									var inputCheckout = '';
									for (var i = 0; i < checkout.length; i++) {
										inputCheckout +='<input type="hidden" value="'+checkout[i]['id']+'" name="id_menu[]">'+
                										'<input type="hidden" value="'+checkout[i]['nama_menu']+'" name="nama_menu[]">'+
																		'<input type="hidden" value="'+checkout[i]['harga_menu']+'" name="harga_menu[]">';
									}
									$(".inputCheckout").html(inputCheckout);
									//dilempar ke form akhir modal yg akan di proses di kontroller
								}

								//aksi button klik menu
								$(document).on('click', '.menuButton', function(){
									var tmp = $(this).attr('id').split('||');
									index_chekout(tmp,1);
								});
								$(document).on('click', '.kurangiPesanan', function(){
									var tmp = $(this).attr('id').split('||');
									index_chekout(tmp,0);
						    });

						</script>

						@endsection
