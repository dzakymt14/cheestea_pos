<div class="modal fade" id="kt_modal_create_app" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered mw-900px">
				<div class="modal-content">
					<div class="modal-header">
						<h2>Masukan Stock</h2>
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<span class="svg-icon svg-icon-1">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
								</svg>
							</span>
						</div>
					</div>
					<div class="modal-body py-lg-10 px-lg-10">
						<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_create_app_stepper">
							<div class="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
								<form class="form" method="POST" action="{{url('/inventory-post')}}">
									@csrf
								<div class="w-100">
									<div class="fv-row mb-10">
										<label class="d-flex align-items-center fs-5 fw-bold mb-2">
											<span class="required">Kategori</span>
											<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Masukan Kategori yang sesuai.."></i>
										</label>
										<select class="form-select form-select-solid" name="nama_kategori" id="kategori" data-placeholder="Pilih atau masukan Kategori baru" data-allow-clear="true">
											<option></option>
											@php
												$kategoris = App\KategoriInventory::all();
											@endphp
											@foreach ($kategoris as $key => $kategori)
												<option value="{{$kategori->nama_kategori}}">{{$kategori->nama_kategori}}</option>
											@endforeach
										</select>
									</div>
									<div class="fv-row">
										<label class="d-flex align-items-center fs-5 fw-bold mb-4">
											<span class="required">Tipe</span>
											<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Pilih Salah satu tipe"></i>
										</label>
										<div class="fv-row">
											<label class="d-flex flex-stack mb-5 cursor-pointer">
												<span class="d-flex align-items-center me-2">
													<span class="symbol symbol-50px me-6">
														<span class="symbol-label bg-light-primary">
															<i class="fas fa-carrot"></i>
														</span>
													</span>
													<span class="d-flex flex-column">
														<span class="fw-bolder fs-6">Perlengkapan</span>
														<span class="fs-7 text-muted">Tipe Stok yang memiliki limitasi penggunaan dan dapat berkurang/habis saat digunakan</span>
													</span>
												</span>
												<span class="form-check form-check-custom form-check-solid">
													<input class="form-check-input" type="radio" name="tipe_inventory" value="perlengkapan" />
												</span>
											</label>
											<label class="d-flex flex-stack mb-5 cursor-pointer">
												<span class="d-flex align-items-center me-2">
													<span class="symbol symbol-50px me-6">
														<span class="symbol-label bg-light-danger">
															<span class="svg-icon svg-icon-1 svg-icon-danger">
																<i class="fas fa-blender"></i>
															</span>
														</span>
													</span>
													<span class="d-flex flex-column">
														<span class="fw-bolder fs-6">Peralatan</span>
														<span class="fs-7 text-muted">Tipe Stok yang dapat digunakan berkali kali dan tidak berkurang jika digunakan</span>
													</span>
												</span>
												<span class="form-check form-check-custom form-check-solid">
													<input class="form-check-input" type="radio" name="tipe_inventory" value="peralatan" />
												</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="flex-row-fluid py-lg-5 px-lg-15">
										<div class="w-100">
											<div class="fv-row">
												<label class="d-flex align-items-center fs-5 fw-bold mb-4">
													<span class="required">Detail</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your apps framework"></i>
												</label>
												<div class="fv-row mb-7">
													<label class="fw-bold fs-6 mb-2">Brand Item</label>
													<input type="text" name="brand_inventory" class="form-control form-control-solid mb-3 mb-lg-0 nama" placeholder="nama brand dari item" />
												</div>
												<div class="mb-10">
													<!--begin::Label-->
													<label class="form-label fw-bold">Satuan</label>
													<!--end::Label-->
													<!--begin::Input-->
													<div class="col-12">
														<select name="satuan_inventory" class="required form-select form-select-solid" data-placeholder="Pilih Satuan Ukuran" data-allow-clear="true">
															<option value="unit">Unit</option>
															<option value="litter">Litter</option>
															<option value="gram">Gram</option>
															<option value="kilogram">Kilogram</option>
														</select>
													</div>
													<!--end::Input-->
												</div>
												<div class="fv-row mb-7">
													<label class="fw-bold fs-6 mb-2">Kuantiti (sesuai satuan)</label>
													<input type="text" name="kuantitas_inventory" class="form-control form-control-solid mb-3 mb-lg-0 nama" placeholder="kuantitas dari item" />
												</div>
												<div class="fv-row mb-7">
													<label class="fw-bold fs-6 mb-2">Kelipatan</label>
													<input type="number" name="kelipatan_item" class="form-control form-control-solid mb-3 mb-lg-0 nama" placeholder="Kelipatan Item yang dimasukan" />
												</div>
												<div class="fv-row mb-7">
													<label class="fw-bold fs-6 mb-2">Harga Total Beli Item</label>
													<input type="text" name="harga_inventory" class="form-control form-control-solid mb-3 mb-lg-0 nama" placeholder="Harga dari item" />
												</div>
											</div>
										</div>



									<div class="d-flex flex-stack pt-10">
										<div class="me-2">
											<button type="reset" id="kt_tambah_data_cancel"  class="btn btn-lg btn-light-primary me-3">
												<i class="fas fa-arrow-left"></i>Batal
											</button>
										</div>
										<div>
											<button type="submit" class="btn btn-lg btn-primary">
												<span class="indicator-label">Simpan
													<i class="fas fa-check"></i>
												</span>
											</button>
										</div>
										<!--end::Wrapper-->
									</div>
									<!--end::Actions-->
								</form>
								<!--end::Form-->
							</div>
							<!--end::Content-->
						</div>
						<!--end::Stepper-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>
		<script type="text/javascript">
		$(document).ready(function() {
			$("#kategori").select2({
				tags: true,
				dropdownParent: $("#kt_modal_create_app"),
				createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  },
		   templateResult: function (data) {
		    var $result = $("<span></span>");

		    $result.text(data.text);

		    if (data.newOption) {
		      $result.append(" <em>(new)</em>");
		    }

		    return $result;
		  }

			});
		});

		</script>
