@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<h1 class="d-flex text-dark fw-bolder my-1 fs-3">Inventory</h1>

								</div>
								<div class="d-flex align-items-center py-2">
									<a href="#kt_modal_create_app" data-bs-toggle="modal" class="btn btn-warning text-dark" id="kt_toolbar_primary_button"><i class="fas fa-plus me-2 text-dark"></i>Tambah Data</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
										<thead>
											<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
												{{-- <th class="w-10px pe-2">
													<div class="form-check form-check-sm form-check-custom form-check-solid me-3">
														<input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_products_table .form-check-input" value="1" />
													</div>
												</th> --}}
												<th class="min-w-200px">Nama</th>
												<th class="text-end min-w-70px">Qty</th>
												<th class="text-end min-w-100px">Status</th>
												<th class="text-end min-w-70px">Actions</th>
											</tr>
										</thead>

										<tbody class="fw-bold text-gray-600">
											@foreach ($kategories as $key => $kategori)
												<tr>
													{{-- <td>
														<div class="form-check form-check-sm form-check-custom form-check-solid">
															<input class="form-check-input" type="checkbox" value="1" />
														</div>
													</td> --}}
													<td>
														<div class="d-flex align-items-center">
															<div class="ms-5">
																<a class="text-gray-800 text-hover-primary fs-5 fw-bolder" data-kt-ecommerce-product-filter="product_name">{{$kategori->nama_kategori}}</a>
															</div>
														</div>
													</td>
													@php
														$litter = App\Inventory::where('id_kategori',$kategori->id)->where('satuan_inventory','litter')->sum('kuantitas_inventory');
														$gram = App\Inventory::where('id_kategori',$kategori->id)->where('satuan_inventory','gram')->sum('kuantitas_inventory');
														$total_kuantitas = $litter+($gram/1000);
													@endphp
													<td class="text-end pe-0" data-order="8">
														@if ($total_kuantitas < 1000)
															<span class="badge badge-light-warning">Low stock</span>
															<br>
															<span class="fw-bolder text-warning ms-3">{{$total_kuantitas}} Litter</span>
														@elseif ($total_kuantitas > 1000)
															<span class="fw-bolder text-success ms-3">{{$total_kuantitas}} Litter</span>
														@elseif ($total_kuantitas==0)
															<span class="badge badge-light-danger">out stock</span>
															<span class="fw-bolder text-danger ms-3">{{$total_kuantitas}} Litter</span>
														@endif
													</td>
													<td class="text-end pe-0" data-order="Inactive">
														<div class="badge badge-light-danger">Inactive</div>
													</td>
													<td>
														<div class="d-flex justify-content-end flex-shrink-0">
															@php
																$item = App\Inventory::where('id_kategori',$kategori->id)->get();
															@endphp
															<a class="showDetailRow btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="{{$item}}" >
																	<i class="fas fa-angle-double-down"></i>
															</a>
														</div>

													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('inventory.modal.modal-tambah-data')

<script type="text/javascript">
function childRow (data) {
	var isi = '';
	for (var i = 0; i < data.length; i++) {
		isi +='<div class="d-flex flex-stack">'+
						'<div class="d-flex align-items-center me-5">'+
							'<div class="me-5">'+
								'<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">'+data[i]['brand_inventory']+'</a>'+
								// '<span class="text-gray-400 fw-bold fs-7 d-block text-start ps-0">234Kg Terpakai</span>'+
							'</div>'+
						'</div>'+
						'<div class="text-gray-400 fw-bolder fs-7 text-end">'+
						'<span class="text-gray-800 fw-bolder fs-6 d-block">'+data[i]['kuantitas_inventory']+' '+data[i]['satuan_inventory']+'</span>'+
						'</div>'+
					'</div>'+
					'<div class="separator separator-dashed my-5"></div>'
	}
	return 	'<table class="table table-row-bordered gy-5">'+
	'<tbody>'+
		'<tr>'+
			'<td></td>'+
			'<td>'+
			isi+
			'</td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
		'</tr>'+
		'</tbody>'+
		'</table>';
}
	$(document).ready(function() {
		var table = $('#kt_datatable_example_1').DataTable( {
			"order": [[ 0, "asc" ]],
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
		// Add event listener for opening and closing details
    $('#kt_datatable_example_1 tbody').on('click', '.showDetailRow', function () {
			var data = JSON.parse($(this).attr('id'));
			console.log(data);
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( childRow(data) ).show();
            tr.addClass('shown');
        }
    } );
	});

</script>

@endsection
