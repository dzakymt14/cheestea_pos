<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Rafif Khosyi">
  <title>Sign In</title>
  <!-- Favicon -->
  <link rel="icon" href="{{asset('argon')}}/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <!-- Icons -->
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->
  <!-- <link rel="stylesheet" href="{{asset('argon')}}/vendor/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/quill/dist/quill.core.css" type="text/css">
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/sweetalert2/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" -->>
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="{{asset('argon')}}/css/argon.css?v=1.1.0" type="text/css">
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/splide-2.4.21/dist/css/splide.min.css" type="text/css">
  <link rel="stylesheet" href="{{asset('argon')}}/vendor/splide-2.4.21/dist/css/themes/splide-default.min.css" type="text/css">
  <link rel="manifest" href="/manifest.json">
  <script src="{{asset('argon')}}/vendor/splide-2.4.21/dist/js/splide.js"></script>
  <script src="{{asset('argon')}}/vendor/splide-2.4.21/dist/js/splide.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>

<body class="g-sidenav-hidden bg-white">
  <div class="main-content">
    @yield('content')
  </div>

  <script src="{{asset('argon')}}/vendor/jquery/dist/jquery.min.js"></script>
  <script src="{{asset('argon')}}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- <script src="{{asset('argon')}}/vendor/js-cookie/js.cookie.js"></script>
  <script src="{{asset('argon')}}/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="{{asset('argon')}}/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="{{asset('argon')}}/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="{{asset('argon')}}/vendor/chart.js/dist/Chart.extension.js"></script>
  <script src="{{asset('argon')}}/vendor/select2/dist/js/select2.min.js"></script>
  <script src="{{asset('argon')}}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="{{asset('argon')}}/vendor/nouislider/distribute/nouislider.min.js"></script>
    <script src="{{asset('argon')}}/vendor/moment/min/moment.min.js"></script>
    <script src="{{asset('argon')}}/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="{{asset('argon')}}/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="{{asset('argon')}}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('argon')}}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('argon')}}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('argon')}}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('argon')}}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('argon')}}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{asset('argon')}}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('argon')}}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/datetime/1.0.3/js/dataTables.dateTime.min.js"></script> -->
    <script src="{{asset('argon')}}/js/argon.js?v=1.1.0"></script>
    @stack('scripts')
</body>

</html>
