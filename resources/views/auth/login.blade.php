@extends('auth.layouts.app-login')

@section('content')

<div class="container-fluid ">
  <div class="split-40 left-0" style="background-color: #ffc700;">
  </div>
  <div class="split-60 right-0">
    <div class="row justify-content-center centered">
      <div class="col-lg-8 mb-2 text-center">
        <img src="{{asset('argon')}}/img/theme/128.png" >
        <h1 class="mb-0">Kasir Hehe</h1>
        <h4 class="mb-0 text-muted text-sm font-weight-400">Silahkan masuk sebelum melakukan transaksi</h4>
      </div>
      <div class="col-lg-7 col-12">
        <form method="post" action="{{ route('login') }}" autocomplete="off" enctype= multipart/form-data>
          @csrf
          <div class="row">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email">Email</label>
                    <input  type="email" class="form-control @error('email') is-invalid @enderror" name="email" required>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-password">Password</label>
                    <input  type="password" class="form-control @error('email') is-invalid @enderror" name="password" required>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-6 col-12">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-control-label"  for="remember">
                          Ingat Saya ?
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="row justify-content-center">
                    <div class="col-lg-12 col-12">
                      <button type="submit" class=" btn btn-block btn-lg mt-3 border-0 shadow-none text-dark" style="background-color: #ffc700;">Login</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
