<div class="modal fade" id="modal_tambah" tabindex="-1" aria-hidden="true">
	<!--begin::Modal dialog-->
	<div class="modal-dialog modal-dialog-centered mw-950px">
		<!--begin::Modal content-->
		<div class="modal-content">
			<!--begin::Modal header-->
			<div class="modal-header" id="kt_modal_add_user_header">
				<!--begin::Modal title-->
				<h2 class="fw-bolder">Tambah Menu</h2>
				<!--end::Modal title-->
				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</div>
				<!--end::Close-->
			</div>
			<!--end::Modal header-->
			<!--begin::Modal body-->
			<div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
				<!--begin::Form-->
				<form id="kt_modal_add_user_form" class="form" action="/menu-post" method="POST" enctype="multipart/form-data">
					@csrf
					<!--begin::Scroll-->
					<div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px">
						<!--begin::Input group-->
						<div class="text-center fv-row mb-7">
							<!--begin::Label-->
							<label class="d-block fw-bold fs-6 mb-5 text-center">Thumbnail</label>
							<!--end::Label-->
							<!--begin::Image input-->
							<div class="image-input image-input-outline text-center" data-kt-image-input="true" style="background-image: url('berkas/menu/dummy.jpg')">
								<!--begin::Preview existing avatar-->
								<div class="image-input-wrapper w-200px h-200px" style="background-image: url(berkas/menu/dummy.jpg);"></div>
								<!--end::Preview existing avatar-->
								<!--begin::Label-->
								<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
									<i class="bi bi-pencil-fill fs-7"></i>
									<!--begin::Inputs-->
									<input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
									<input type="hidden" name="avatar_remove" />
									<!--end::Inputs-->
								</label>
								<!--end::Label-->
								<!--begin::Cancel-->
								<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
									<i class="bi bi-x fs-2"></i>
								</span>
								<!--end::Cancel-->
								<!--begin::Remove-->
								<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
									<i class="bi bi-x fs-2"></i>
								</span>
								<!--end::Remove-->
							</div>
							<!--end::Image input-->
							<!--begin::Hint-->
							<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
							<!--end::Hint-->
						</div>
						<div class="row mb-5">
							<div class="col-md-6 fv-row">
								<label class="required fs-5 fw-bold mb-2">Nama Produk</label>
								<input type="text" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="nama_menu" />
							</div>
							<div class="col-md-6 fv-row">
								<label class="required fs-5 fw-bold mb-2">Harga</label>
								<input type="number" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="harga_menu" />
							</div>
						</div>
						<!--end::Input group-->
						<!--begin::Input group-->
						<div class="row mb-7">
							<!--begin::Label-->
							<label class="required fw-bold fs-6 mb-5">Tipe</label>
							<div class="col-md-4">
								<div class="fv-row">
									<div class="form-check form-check-custom form-check-solid">
										<input class="form-check-input me-3" name="tipe_menu" type="radio" value="minum" id="kt_modal_update_role_option_0" checked='checked' />
										<label class="form-check-label" for="kt_modal_update_role_option_0">
											<div class="fw-bolder text-gray-800">Minuman</div>
											<div class="text-gray-600">Untuk Produk tipe Minuman</div>
										</label>
									</div>
								</div>
							</div>
							<div class='separator separator-dashed my-5'></div>
								<div class="col-md-4">
									<div class="fv-row">
										<div class="form-check form-check-custom form-check-solid">
											<input class="form-check-input me-3" name="tipe_menu" type="radio" value="makan" id="kt_modal_update_role_option_0" checked/>
											<label class="form-check-label" for="kt_modal_update_role_option_0">
												<div class="fw-bolder text-gray-800">Makanan</div>
												<div class="text-gray-600">Untuk Produk tipe Makanan</div>
											</label>
										</div>
									</div>
								</div>
							<div class='separator separator-dashed my-5'></div>
							<div class="col-md-4">
								<div class="fv-row">
									<div class="form-check form-check-custom form-check-solid">
										<input class="form-check-input me-3" name="tipe_menu" type="radio" value="add" id="kt_modal_update_role_option_0" checked/>
										<label class="form-check-label" for="kt_modal_update_role_option_0">
											<div class="fw-bolder text-gray-800">Add on</div>
											<div class="text-gray-600">Untuk Produk tipe Tambahan</div>
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class='separator separator-dashed my-5'></div>
						<h5>komposisi</h5>
						<hr>
						<div class="row mb-5">
							<div class="col-md-5 fv-row">
								<label class="fs-5 fw-bold mb-2">Katagori</label>
								<select class="form-select form-select-solid kategori" name="kategori_komposisi[]" data-placeholder="Pilih atau masukan Kategori baru" data-allow-clear="true">
									<option></option>
									@php
										$kategoris = App\KategoriInventory::all();
									@endphp
									@foreach ($kategoris as $key => $kategori)
										<option value="{{$kategori->nama_kategori}}">{{$kategori->nama_kategori}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-2 fv-row">
								<label class="fs-5 fw-bold mb-2">qty</label>
								<input type="number" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="jumlah_komposisi[]" />
							</div>
							<div class="col-md-2 fv-row">
								<label class="fs-5 fw-bold mb-2">Satuan</label>
								<select name="satuan_komposisi[]" class="required form-select form-select-solid" data-placeholder="Pilih Satuan Ukuran" data-allow-clear="true">
									<option value="ml">ml</option>
									<option value="gram">gram</option>
								</select>
							</div>
							<div class="col-md-2 text-center">
								<a href="#!" class="btn_tambah_komposisi"><i class="fas fa-plus text-success me-2"></i></a>
								{{-- <a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a>	 --}}
							</div>
						</div>
						<div class="new-komposisi"></div>
						<!--end::Input group-->
					</div>
					<!--end::Scroll-->
					<!--begin::Actions-->
					<div class="text-center pt-15">
						<button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
							<span class="indicator-label">Tambah</span>
							<span class="indicator-progress">Mohon Tunggu...
							<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
						</button>
					</div>
					<!--end::Actions-->
				</form>
				<!--end::Form-->
			</div>
			<!--end::Modal body-->
		</div>
		<!--end::Modal content-->
	</div>
	<!--end::Modal dialog-->
</div>
<script>
$(document).ready(function() {
	$(".kategori").select2({
		tags: true,
		dropdownParent: $("#modal_tambah"),
		createTag: function (params) {
		return {
			id: params.term,
			text: params.term,
			newOption: true
		}
	},
	 templateResult: function (data) {
		var $result = $("<span></span>");

		$result.text(data.text);

		if (data.newOption) {
			$result.append(" <em>(new)</em>");
		}

		return $result;
	}

	});
});
	let by = 1;
    tambahKompisi(by);
    function tambahKompisi(number) {
        // let pilih = 'selectpicker';
        let CRtambahKompisi = '';
            CRtambahKompisi += '<div class ="row mb-5" id ="tambahCameraNew">'+
				'<div class="col-md-5 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Katagori</label>'+
					'<select class="form-select form-select-solid" name="kategori_komposisi[]" >'+
						'<option></option>@php$kategoris = App\KategoriInventory::all();@endphp'+
						'@foreach ($kategoris as $key => $kategori)<option value="{{$kategori->nama_kategori}}">{{$kategori->nama_kategori}}</option>@endforeach'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">qty</label>'+
					'<input type="number" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="jumlah_komposisi[]" />'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Satuan</label>'+
					'<select name="satuan_komposisi[]" class="required form-select form-select-solid" data-placeholder="Pilih Satuan Ukuran" data-allow-clear="true">'+
						'<option value="ml">ml</option>'+
						'<option value="gram">gram</option>'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 text-center mb-5">'+
					'<a href="#!" class="btn_tambah_komposisi"><i class="fas fa-plus text-success me-2"></i></a>'+
					'<a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a>'+
				'</div>'+
			'</div>';
        if (number > 1) {
            $('.new-komposisi').append(CRtambahKompisi);
        }
    }
    $(document).on('click', '.btn_tambah_komposisi', function() {
        by++;
        tambahKompisi(by);
        // $('.pilih').selectpicker();
    });
    $(document).on('click', '.remove', function(){
        by--;
        $(this).closest("#tambahCameraNew").remove();
    });
</script>
