@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<h1 class="d-flex text-dark fw-bolder my-1 fs-3">Menu</h1>

									{{-- <div class="card-toolbar">
										<ul class="nav">
											<li class="nav-item">
												<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary active fw-bolder px-4 me-1" data-bs-toggle="tab" href="#kt_table_widget_7_tab_1">Gerai Aktif</a>
											</li>
											<li class="nav-item">
												<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1" data-bs-toggle="tab" href="#kt_table_widget_7_tab_2">Gerai Non Aktif</a>
											</li>
										</ul>
									</div> --}}
								</div>
								<div class="d-flex align-items-center py-2">
									<a href="#modal_tambah" data-bs-toggle="modal" class="btn btn-warning text-dark" id="kt_toolbar_primary_button"><i class="fas fa-plus me-2 text-dark"></i>Tambah Menu</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
							<thead>
								<tr class="fw-bolder text-muted">
									<th class="min-w-150px">Nama Menu</th>
									<th class="min-w-150px">Tipe</th>
									<th class="min-w-150px">Penjualan</th>
									<th class="min-w-120px">Rating</th>
									<th class="min-w-100px text-end">Actions</th>
								</tr>
							</thead>
							<!--end::Table head-->
							<!--begin::Table body-->
							<tbody>
								@foreach ($menus as $key => $menu)
									<tr>
										<td>
											<div class="d-flex align-items-center">
												<div class="symbol symbol-45px me-5">
													<img src="{{('berkas')}}/menu/{{$menu->foto_menu}}" alt="" />
												</div>
												<div class="d-flex justify-content-start flex-column">
													<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">{{$menu->nama_menu}}</a>
													<span class="text-muted fw-bold text-muted d-block fs-7">Rp.{{number_format($menu->harga_menu,0)}}</span>
												</div>
											</div>
										</td>
										<td>
											@if ($menu->tipe_menu == 'minum')
												<div class="badge badge-light-success fw-bolder">Minuman</div>
											@elseif ($menu->tipe_menu == 'makan')
												<div class="badge badge-light-primary fw-bolder">Makanan</div>
											@elseif ($menu->tipe_menu == 'add')
												<div class="badge badge-light-info fw-bolder">Add on</div>
											@endif
										</td>
										<td class="text-end">
											<div class="d-flex flex-column w-100 me-2">
												<div class="d-flex flex-stack mb-2">
													<span class="me-2 fs-7 fw-bold">100 kali</span>
												</div>
											</div>
										</td>
										<td class="text-end">
											<div class="d-flex flex-column w-100 me-2">
												<div class="d-flex flex-stack mb-2">
													<span class="me-2 fs-7 fw-bold">56%</span>
												</div>
											</div>
										</td>
										<td>
											<div class="d-flex justify-content-end flex-shrink-0">
												<a href="#modalUbahDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk mengubah data">
													<i class="fas fa-edit"></i>
												</a>
												<a href="#modalHapusDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk menghapus karyawan">
													<i class="fas fa-trash"></i>
												</a>
												@php
													$item = App\komposisi::where('menu_id',$menu->id)->get();
												@endphp
												<button  class="showDetailRow btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="{{$item}}">
													<i class="fas fa-angle-double-down"></i>
												</button>
											</div>

										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('menu.modal.modal-tambah-data')

<script type="text/javascript">
	$(document).ready(function() {
		function childRowKosong(){
			return 'tidak ada komposisi';
		}
		function childRow (data) {
			var isi = '';
			for (var i = 0; i < data.length; i++) {
				isi +='<div class="d-flex flex-stack">'+
								'<div class="d-flex align-items-center me-5">'+
									'<div class="me-5">'+
										'<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">'+data[i]['kategori_komposisi']+'</a>'+
										// '<span class="text-gray-400 fw-bold fs-7 d-block text-start ps-0">234Kg Terpakai</span>'+
									'</div>'+
								'</div>'+
								'<div class="text-gray-400 fw-bolder fs-7 text-end">'+
								'<span class="text-gray-800 fw-bolder fs-6 d-block">'+data[i]['jumlah_komposisi']+' '+data[i]['satuan_komposisi']+'</span>'+
								'</div>'+
							'</div>'+
							'<div class="separator separator-dashed my-5"></div>'
			}
			return 	'<table class="table table-row-bordered gy-5">'+
			'<tbody>'+
				'<tr>'+
					'<td></td>'+
					'<td>'+
					isi+
					'</td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
				'</tr>'+
				'</tbody>'+
				'</table>';
		}
		var table = $('#kt_datatable_example_1').DataTable( {
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
		// Add event listener for opening and closing details
    $('#kt_datatable_example_1 tbody').on('click', '.showDetailRow', function () {
			var data = JSON.parse($(this).attr('id'));
			console.log(data);
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
						if (data.length == 0) {
							row.child( childRowKosong() ).show();
							tr.addClass('shown');
						}else {
							row.child( childRow(data) ).show();
	            tr.addClass('shown');
						}

        }
    } );
	});
</script>

@endsection
