<div class="modal fade" id="modalHapusDataKaryawan" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <form class="form" action="#" id="kt_tambah_data_form">
                        <div class="modal-header" id="kt_tambah_data_header">
                            <h3 class="modal-title">Hapus Data</h3>
                            <div class="btn btn-sm btn-icon btn-active-warning btn-color-muted" data-bs-dismiss="modal">
                                <i class="fas fa-times"></i>
                            </div>
                        </div>
                        <div class="modal-body py-10 px-lg-17">
                            <!--begin::Scroll-->
                            <div class="scroll-y me-n7 pe-7" id="kt_tambah_data_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_tambah_data_header" data-kt-scroll-wrappers="#kt_tambah_data_scroll" data-kt-scroll-offset="300px">
                                <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                                    <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                                        </svg>
                                    </span>
                                    <div class="d-flex flex-stack flex-grow-1">
                                        <div class="fw-bold">
                                            <h5 class="text-gray-900 fw-bolder mb-0">Perhatian</h5>
                                            <div class="fs-6 text-gray-700">1. Data yang anda hapus hanya akan terhapus pada gerai</div>
                                            <div class="fs-6 text-gray-700">1. Karyawan tetap ada pada sistem namun tidak lagi bisa mengakses gerai</div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div>
                        <div class="modal-footer flex-center">
                            <button type="reset" id="kt_tambah_data_cancel" class="btn btn-light me-3">Batal</button>
                            <button type="submit" id="kt_tambah_data_submit" class="btn btn-warning">
                                <span class="indicator-label text-dark">Lanjutkan</span>
                                <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
