@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<h1 class="d-flex text-dark fw-bolder my-1 fs-3">Karyawan</h1>

								</div>
								<div class="d-flex align-items-center py-2">
									<a href="#modal_tambah" data-bs-toggle="modal" class="btn btn-warning text-dark createButton" id="kt_toolbar_primary_button"><i class="fas fa-plus me-2 text-dark"></i>Tambah Data</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
										<thead>
											<tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
												<th class="min-w-125px">Nama Karyawan</th>
												<th class="min-w-125px">Role</th>
												<th class="min-w-125px">Login Terakhir</th>
												<th class="min-w-125px">Status</th>
												<th class="text-end min-w-100px">Actions</th>
											</tr>
										</thead>
										<tbody class="text-gray-600 fw-bold">
											@foreach ($karyawans as $key => $karyawan)
												<tr>
													<td class="d-flex align-items-center">
														<div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
															<a>
																<div class="symbol-label">
																	<img src="{{asset('berkas')}}/karyawan/{{$karyawan->foto_user}}" class="w-100" />
																</div>
															</a>
														</div>
														<div class="d-flex flex-column">
															<a class="text-gray-800 text-hover-primary mb-1">{{$karyawan->nama}}</a>
															<span>{{$karyawan->email}}</span>
														</div>
													</td>
													<td>{{$karyawan->level}}</td>
													<td>
														<div class="badge badge-light fw-bolder">Yesterday</div>
													</td>
													<td><div class="badge @if ($karyawan->status_user=="aktif") badge-light-success @else badge-light-danger @endif fw-bolder">{{$karyawan->status_user}}</div></td>
													<td class="text-end">
														<a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
														<span class="svg-icon svg-icon-5 m-0">
															<i class="fas fa-angle-down"></i>
														</span>
														</a>
														<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
															@if ($karyawan->status_user=="aktif")
																<div class="menu-item px-3">
																	<a class="menu-link px-3 editButton" href="#modal_tambah" data-bs-toggle="modal" id="{{$karyawan->id}}||{{$karyawan->foto_user}}||
																			||{{$karyawan->nama}}||{{$karyawan->email}}||{{$karyawan->no_tlpn}}" onclick="setData(this.id)">Edit</a>
																</div>
																<div class="menu-item px-3">
																	<a class="menu-link px-3 nonAktifButton" href="#modalNonAktifKaryawan" data-bs-toggle="modal" id="{{$karyawan->id}}" onclick="setData(this.id)">Non Aktifkan</a>
																</div>
														 @endif
														</div>
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('karyawan.modal.modal-tambah-data')
@include('karyawan.modal.modal-non-aktif-karyawan')

<script type="text/javascript">
	$(document).ready(function() {
		$("#kt_datatable_example_2").DataTable({
			"scrollY": "170px",
			"scrollCollapse": true,
			"paging": false,
			"dom": "<'table-responsive'tr>"
		});
		$('#kt_datatable_example_1').DataTable( {
			"order": [[ 0, "asc" ]],
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );

		$(".createButton").click(function(){
			$('.code_post').val(1);
			$('.judulForm').text(function(i, oldText) {
        return oldText === 'Detail Karyawan' ? 'Tambah Karyawan' : oldText;
    	});
			$('.foto_user').css("background-image", "url(assets/media/avatars/blank.png)");
			$('.judul_foto_user').val('');
			$('.id_user').val('');
			$('.nama').val('');
			$('.email').val('');
			$('.no_tlpn').val('');
		});



		$(".editButton").click(function(){
			$('.code_post').val(2);
			$('.judulForm').text(function(i, oldText) {
        return oldText === 'Tambah Karyawan' ? 'Detail Karyawan' : oldText;
    	});
		});

		$(".nonAktifButton").click(function(){
			$('.code_post').val(3);
		});
		$(".removeAvatar").click(function(){
			$('.judul_foto_user').val(null);
		});
		$('input[type="file"]').change(function(e) {
        let fileName = e.target.files[0].name;
				$('.judul_foto_user').val(fileName);
      });

	});

	function setData(id) {
			var data = id.split('||');
			$('.foto_user').css("background-image", "url(berkas/karyawan/"+data[1]+")");
			$('.judul_foto_user').val(data[1]);
			$('.id_user').val(data[0]);
			$('.nama').val(data[3]);
			$('.email').val(data[4]);
			$('.no_tlpn').val(data[5]);
			console.log(data);
		}
</script>

@endsection
