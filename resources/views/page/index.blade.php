@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<h1 class="d-flex text-dark fw-bolder my-1 fs-3">Index</h1>
									<ul class="breadcrumb breadcrumb-dot fw-bold text-gray-600 fs-7 my-1">
										<li class="breadcrumb-item text-gray-600">
											<a href="/dashboard" class="text-gray-600 text-hover-primary">Home</a>
										</li>
										<li class="breadcrumb-item text-gray-500">Index</li>
									</ul>
								</div>
								<div class="d-flex align-items-center py-2">
									<a href="#modal-tambah" data-bs-toggle="modal" class="btn btn-warning text-dark" id="kt_toolbar_primary_button"><i class="fas fa-plus me-2 text-dark"></i>Tambah Data</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
							<thead>
								<tr class="fw-bold fs-7 text-muted">
									<th>#</th>
									<th>Produk</th>
									<th>Stack</th>
									<th>Active Users</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="align-middle text-muted">1</td>
									<td>
										<div class="d-flex align-items-sm-center">
											<div class="symbol symbol-50px me-5">
												<span class="symbol-label">
													<img src="{{asset('assets')}}/media/svg/brand-logos/bebo.svg" class="h-50 align-self-center" alt="">
												</span>
											</div>
											<div class="d-flex align-items-center flex-row-fluid flex-wrap">
												<div class="flex-grow-1 me-2">
													<a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder">Active Customers</a>
													<span class="text-muted fw-bold d-block fs-6">Mark, Rowling, Esther</span>
												</div>
											</div>
										</div>
									</td>
									<td>
										<span class="badge badge-light-primary fw-bold me-1 fs-6">Angular</span>
										<span class="badge badge-light-info fw-bold me-1 fs-6">PHP</span>
									</td>
									<td>
										<span class="fw-bold fs-6">4600 Users</span>
									</td>
									<td class="text-end">
										<a href="#" class="btn btn-sm btn-icon btn-light-dark">
											<i class="fas fa-arrow-right"></i>
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('page.add.modal-tambah')

<script type="text/javascript">
	$(document).ready(function() {
		$("#kt_datatable_example_2").DataTable({
			"scrollY": "170px",
			"scrollCollapse": true,
			"paging": false,
			"dom": "<'table-responsive'tr>"
		});
		$('#kt_datatable_example_1').DataTable( {
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
	});
</script>

@endsection