@extends('layouts.app')

@section('content')
<div class="d-flex flex-column flex-root">
	<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed pt-20" style="background-image: url(assets/media/illustrations/sketchy-1/14.png">
		<div class="d-flex flex-column flex-center p-10 p-lg-20">
			<div class="card shadow-none bg-white">
				<div class="card-body">
						<a href="#!" onclick="history.back()" class="h5 fs-6"><u><i class="fas fa-arrow-left me-2"></i>Kembali</u></a>
					<div class="d-flex flex-column flex-center">
				<h4 class="fw-bolder fs-2qx text-dark mt-4 mb-0">PIN ANDA</h4>
				<span class="text-muted mb-7">Silahkan masukkan PIN anda sebelum melakukan transaksi</span>
				<form class="form fv-row" id="kt_coming_soon_form">
					<div class="d-flex flex-center">
						<input class="form-control form-control form-control-solid w-md-250px m-2" type="text" name="pin" placeholder="****" required autocomplete="off" />
						<button type="button" id="kt_coming_soon_submit" class="btn btn-warning fw-bolder m-2 text-nowrap">
							<span class="indicator-label text-dark">Submit</span>
							<span class="indicator-progress">Please wait...
								<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
							</button>
						</div>
					</form>
				</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@endsection