<div class="modal fade" tabindex="-1" id="modal-absen">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header py-3">
                <h3 class="modal-title">Absen</h3>
                <div class="btn btn-icon btn-sm btn-active-light-warning ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="modal-body mb-4">
                <div class="mb-2">
                    <h1 class="fw-bold text-gray-800 text-center lh-lg">Apakah anda
                        <br>telah melakukan
                        <span class="fw-boldest text-warning">Absen ?</span></h1>
                        <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center card-rounded-bottom h-200px mh-200px my-5 my-lg-12" style="background-image:url('assets/media/svg/illustrations/easy/1.svg')"></div>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-warning me-2 text-dark"><i class="fas fa-camera me-2 text-dark"></i>Ambil Foto</a>
                        <a class="btn btn-light text-dark" href="#!">12:23:00</a>
                    </div>
                </div>
            </div>
        </div>
    </div>