<div class="modal fade" tabindex="-1" id="modal-bayar">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header py-3">
                <h5 class="modal-title">Detail Pembayaran</h5>
                <div class="btn btn-icon btn-sm btn-active-light-warning ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="modal-body scroll-y mx-5 mx-xl-18 pt-0">
                <div class="mb-5 mt-5">
                    <span class="text-gray-600 fs-6 fw-bold"><i class="fas fa-file me-2"></i>Order #<b class="text-dark">32324</b></span>
                    <br>
                    <span class="text-gray-600 fs-6 fw-bold"><i class="fas fa-calendar-alt me-2"></i>Tanggal : <b class="text-dark">4-4-2022</b></span>
                </div>
                <div class="mb-15">
                    <div class="mh-200px scroll-y me-n7 pe-7">
                        @for($i=0;$i<4;$i++)
                        <div class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0 fw-boldest">x 23</h5>
                                <div class="ms-6">
                                    <span class="d-flex align-items-center fs-5 fw-bolder text-dark">Nama Produk
                                    </span>
                                    <span class="badge badge-light-primary fs-8 fw-bold">Kategori</span>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="text-end">
                                    <div class="fs-5 fw-bolder text-dark">Rp. 23.000</div>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <div class="fw-bold">
                        <label class="fs-2 fw-boldest">Total :</label>
                        <div class="fs-7 text-muted">Harga yang tertera belum termasuk diskon. Mohon diperhatikan</div>
                    </div>
                    <span class="badge badge-dark border border-dashed fs-2 fw-boldest text-warning p-3">
                        <span class="fs-6 fw-bold text-white mb-0">Rp</span> 20.0000</span>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="row">
                            <div class="col-xl-6">
                                <label for="diskon" class="required form-label">Metode Pembayaran</label>
                                <select class="form-select" aria-label="Select example">
                                    <option></option>
                                    <option value="1">Cash</option>
                                    <option value="2">E-Money</option>
                                    <option value="3">Debit/Credit</option>
                                </select>

                            </div>
                            <div class="col-xl-6">
                                <label for="diskon" class="form-label">Diskon <span class="fs-9 text-muted">(%)</span></label>
                                <input type="number" class="form-control" placeholder="%"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer flex-center border-0">
                    <button type="reset" id="kt_tambah_data_cancel" class="btn btn-light me-3">Discard</button>
                    <button type="submit" id="kt_tambah_data_submit" class="btn btn-warning">
                        <span class="indicator-label text-dark">Bayar Rp. 20.000</span>
                        <span class="indicator-progress">Please wait...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                    </div>
                </div>
            </div>

        </div>