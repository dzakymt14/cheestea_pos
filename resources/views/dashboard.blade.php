@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
					<div class="col-xl-12">
						<div class="row gy-0 gx-10">
							<div class="col-12">
								<div class="row">
									<div class="col-xl-8 col-6">
										<ul class="nav row mb-8">
											<li class="nav-item col-4 mb-5 mb-lg-0">
												<a class="nav-link btn btn-flex btn-color-muted btn-outline btn-outline-default btn-active-dark d-flex flex-grow-1 flex-column flex-center py-8 active" data-bs-toggle="tab" href="#kt_general_widget_1_1">
													<span class="svg-icon svg-icon-3x mb-5 mx-0">
														<i class="fas fa-hamburger fs-1"></i>
													</span>
													<span class="fs-6 fw-bold">Makanan</span>
												</a>
											</li>
											<li class="nav-item col-4 mb-5 mb-lg-0">
												<a class="nav-link btn btn-flex btn-color-muted btn-outline btn-outline-default btn-active-dark d-flex flex-grow-1 flex-column flex-center py-8" data-bs-toggle="tab" href="#kt_general_widget_1_2">
													<span class="svg-icon svg-icon-3x mb-5 mx-0">
														<i class="fas fa-coffee fs-1"></i>
													</span>
													<span class="fs-6 fw-bold">Minuman</span>
												</a>
											</li>
											<li class="nav-item col-4 mb-5 mb-lg-0">
												<a class="nav-link btn btn-flex btn-color-muted btn-outline btn-outline-default btn-active-dark d-flex flex-grow-1 flex-column flex-center py-8" data-bs-toggle="tab" href="#kt_general_widget_1_3">
													<span class="svg-icon svg-icon-3x mb-5 mx-0">
														<i class="fas fa-utensils fs-1"></i>
													</span>
													<span class="fs-6 fw-bold">Add On</span>
												</a>
											</li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane fade active show" id="kt_general_widget_1_1">
												<div class="row">
													<div class="col-12 mb-3">
														<h5>Total : 12</h5>
													</div>
													@for($i=0;$i<8;$i++)
													<div class="col-xl-3 col-md-4 mb-5">
														<a href="#!">
															<div class="card-xl-stretch card-rounded border">
																<div class="card-body">
																	<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('assets')}}/media/stock/600x400/img-15.jpg')"></div>
																	<div class="mt-5">
																		<h5 class="fw-bolder text-dark mb-0">Nama Barang</h5>
																		<span class="fs-6 fw-bolder text-gray-600">
																			<span class="fs-7 fw-bold text-gray-600">Rp</span> 20.000</span>
																		</div>
																	</div>
																</div>
															</a>
														</div>
														@endfor
													</div>
												</div>
												<div class="tab-pane fade" id="kt_general_widget_1_2">
													<div class="row">
														<div class="col-12 mb-3">
															<h5>Total : 12</h5>
														</div>
														@for($i=0;$i<8;$i++)
														<div class="col-xl-3 col-md-4 mb-5">
															<a href="#!">
																<div class="card-xl-stretch card-rounded border">
																	<div class="card-body">
																		<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('assets')}}/media/stock/600x400/img-19.jpg')"></div>
																		<div class="mt-5">
																			<h5 class="fw-bolder text-dark mb-0">Nama Barang</h5>
																			<span class="fs-6 fw-bolder text-gray-600">
																				<span class="fs-7 fw-bold text-gray-600">Rp</span> 20.000</span>
																			</div>
																		</div>
																	</div>
																</a>
															</div>
															@endfor
														</div>
													</div>
													<div class="tab-pane fade" id="kt_general_widget_1_3">
														<div class="row">
															<div class="col-12 mb-3">
																<h5>Total : 12</h5>
															</div>
															@for($i=0;$i<8;$i++)
															<div class="col-xl-3 col-md-4 mb-5">
																<a href="#!">
																	<div class="card-xl-stretch card-rounded border">
																		<div class="card-body">
																			<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('assets')}}/media/stock/600x400/img-12.jpg')"></div>
																			<div class="mt-5">
																				<h5 class="fw-bolder text-dark mb-0">Nama Barang</h5>
																				<span class="fs-6 fw-bolder text-gray-600">
																					<span class="fs-7 fw-bold text-gray-600">Rp</span> 20.000</span>
																				</div>
																			</div>
																		</div>
																	</a>
																</div>
																@endfor
															</div>
														</div>
													</div>
												</div>
												<div class="col-xl-4 col-6" style="position:fixed; right: 0;">
													<div class="card card-flush py-4 flex-row-fluid">
														<div class="card-body py-3">
															<div class="row">
																<div class="col-12 mb-4">
																	<h2 class="fw-bolder mb-0">Order Details (<b class="text-warning">#14534</b>)</h2>
																	<i class="fas fa-calendar-alt me-2"></i><span class="text-gray-600 fs-7">Tanggal : <b class="text-dark">4-4-2022</b></span>
																</div>
																<div class="col-12">
																	<hr class="mt-2 text-gray-600">
																	<table id="kt_datatable_example_2" class="table table-row-bordered gs-7">
																		<thead>
																			<tr class="fw-bold fs-6 text-gray-800" style="display: none;">
																				<th>Produk</th>
																			</tr>
																		</thead>
																		<tbody>
																			@for($i=0;$i<4;$i++)
																			<tr>
																				<td>
																					<div class="row align-items-center">
																						<div class="col-xl-9 col-6">
																							<div class="d-flex align-items-center">
																								<h5 class="mb-0 fw-bolder">x 23</h5>
																								<div class="ms-5">
																									<h5 class="mb-0">Nama Barang</h5>
																									<div>
																										<h6 class="mb-0 text-gray-600 fw-bold">Rp. 20.0000</h6>
																									</div>
																								</div>
																							</div>
																						</div>
																						<div class="col-xl-3 col-6 text-center">
																							<a href="#!" ><i class="fas fa-plus text-success me-2"></i></a>
																							<a href="#!" ><i class="fas fa-trash text-danger"></i></a>	
																						</div>
																					</div>
																				</td>
																			</tr>
																			@endfor
																		</tbody>
																	</table>
																</div>
																<div class="col-12 mt-4">
																	<h5 class="mb-3">Total :</h5>
																	<div class="fs-6 fw-bolder d-flex flex-stack">
																		<span class="badge badge-dark border border-dashed fs-2 fw-boldest text-warning p-3">
																			<span class="fs-6 fw-bold text-white">Rp</span> 20.0000</span>
																			<a href="#modal-bayar" data-bs-toggle="modal" class="btn btn-warning text-dark"><i class="fas fa-wallet me-2 text-dark"></i>Bayar</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>

						@include('page.add.modal-bayar')

						<script type="text/javascript">
							$(document).ready(function() {
								$("#kt_datatable_example_2").DataTable({
									"scrollY": "170px",
									"scrollCollapse": true,
									"paging": false,
									"dom": "<'table-responsive'tr>"
								});
								$("#kt_datatable_example_3").DataTable({
									"scrollY": "200px",
									"scrollCollapse": true,
									"paging": false,
									"dom": "<'table-responsive'tr>"
								});
							});
						</script>

						@endsection