@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
@if (session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif
	<div class="toolbar d-flex flex-stack mb-0 mb-lg-n4 pt-5" id="kt_toolbar">
						<div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
							<div class="page-title d-flex flex-column me-3">
								<h1 class="d-flex text-dark fw-bolder my-1 fs-3">Gerai Spaku</h1>
							</div>
						</div>
					</div>
	<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
							<!--begin::Post-->
							<div class="content flex-row-fluid" id="kt_content">
								<!--begin::Row-->
								<div class="row g-5 g-xl-10 mb-xl-10">
									<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">
										{{-- ini untuk card penjualan produk hari ini --}}
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<div class="d-flex align-items-center">
														<span class="fs-4 fw-bold text-gray-400 me-1 align-self-start">Rp</span>
														<span class="fs-2hx fw-bolder text-dark me-2 lh-1">1,250,000</span>
														<span class="badge badge-success fs-6 lh-1 py-1 px-2 d-flex flex-center" style="height: 22px">
														<span class="svg-icon svg-icon-7 svg-icon-white ms-n1">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<path opacity="0.5" d="M13 9.59998V21C13 21.6 12.6 22 12 22C11.4 22 11 21.6 11 21V9.59998H13Z" fill="black" />
																<path d="M5.7071 7.89291C5.07714 8.52288 5.52331 9.60002 6.41421 9.60002H17.5858C18.4767 9.60002 18.9229 8.52288 18.2929 7.89291L12.7 2.3C12.3 1.9 11.7 1.9 11.3 2.3L5.7071 7.89291Z" fill="black" />
															</svg>
														</span>
														<!--end::Svg Icon-->2.2%</span>
													</div>
													<span class="text-gray-400 pt-1 fw-bold fs-6">Penjualan Produk hari ini</span>
												</div>
											</div>
											<div class="card-body pt-2 pb-4 d-flex align-items-center">
												<div class="d-flex flex-center me-5 pt-2">
													<canvas id="penjualan_produk_hari_ini_chart" style="min-width: 50px; min-height: 50px" ></canvas>
												</div>
												<div class="d-flex flex-column content-justify-center w-100">
													<div class="d-flex fs-6 fw-bold align-items-center">
														<div class="bullet w-8px h-6px rounded-2 bg-primary me-3"></div>
														<div class="text-gray-500 flex-grow-1 me-4">50 Minuman</div>
														<div class="fw-boldest text-gray-700 text-xxl-end">Rp.1,000,000</div>
													</div>
													<div class="d-flex fs-6 fw-bold align-items-center my-3">
														<div class="bullet w-8px h-6px rounded-2 bg-danger me-3"></div>
														<div class="text-gray-500 flex-grow-1 me-4">4 Makanan</div>
														<div class="fw-boldest text-gray-700 text-xxl-end">Rp.150,000</div>
													</div>
													<div class="d-flex fs-6 fw-bold align-items-center">
														<div class="bullet w-8px h-6px rounded-2 bg-success me-3"></div>
														<div class="text-gray-500 flex-grow-1 me-4">Add on</div>
														<div class="fw-boldest text-gray-700 text-xxl-end">Rp.100,000</div>
													</div>
												</div>
											</div>
										</div>
										{{-- end untuk card penjualan produk hari ini --}}
										{{-- absen karyawan gerai --}}
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header align-items-center border-0">
												<h3 class="fw-bolder text-gray-900 m-0">Absen Karyawan Gerai</h3>
												<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
													<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
															<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
														</svg>
													</span>
												</button>
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
													<div class="menu-item px-3">
														<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
													</div>
													<div class="separator mb-3 opacity-75"></div>
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3">Tarik Absen Sebulan (Excel)</a>
													</div>
												</div>
											</div>
											<div class="card-body pt-2">
												<ul class="nav nav-pills nav-pills-custom mb-3">
													<li class="nav-item mb-3 me-3 me-lg-6">
														<a class="nav-link d-flex justify-content-between flex-column flex-center overflow-hidden active w-80px h-85px py-4" data-bs-toggle="pill" href="#suketi">
															<div class="nav-icon">
																<img alt="" src="{{('berkas')}}/karyawan/suketi.jpg" class="" />
															</div>
															<span class="nav-text text-gray-700 fw-bolder fs-6 lh-1">Suketi</span>
															<span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-primary"></span>
														</a>
													</li>
													<li class="nav-item mb-3 me-3 me-lg-6">
														<a class="nav-link d-flex justify-content-between flex-column flex-center overflow-hidden w-80px h-85px py-4" data-bs-toggle="pill" href="#tukiyem">
															<div class="nav-icon">
																<img alt="" src="{{('berkas')}}/karyawan/tukiyem.jpg" class="" />
															</div>
															<span class="nav-text text-gray-700 fw-bolder fs-6 lh-1">Tukiyem</span>
															<span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-primary"></span>
														</a>
													</li>
												</ul>

												<div class="tab-content">
													<div class="tab-pane fade show active" id="suketi">
														<div class="table-responsive">
															<table class="table table-row-dashed align-middle gs-0 gy-4 my-0">
																<thead>
																	<tr class="fs-7 fw-bolder text-gray-500 border-bottom-0">
																		<th class="ps-0 w-50px">#</th>
																		<th class="min-w-140px">Status</th>
																		<th class="min-w-140px">Waktu</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>1</td>
																		<td>
																			<span class="text-gray-800 fw-bolder text-end">Clock In 1</span>
																		</td>
																		<td>
																			<span class="text-gray-800 fw-bolder d-block fs-6">10:00 AM</span>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="tab-pane fade" id="tukiyem">
														<div class="table-responsive">
															<table class="table table-row-dashed align-middle gs-0 gy-4 my-0">
																<thead>
																	<tr class="fs-7 fw-bolder text-gray-500 border-bottom-0">
																		<th class="ps-0 w-50px">#</th>
																		<th class="min-w-140px">Status</th>
																		<th class="min-w-140px">Waktu</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>1</td>
																		<td>
																			<span class="text-gray-800 fw-bolder text-end">Clock In 1</span>
																		</td>
																		<td>
																			<span class="text-gray-800 fw-bolder d-block fs-6">08:00 AM</span>
																		</td>
																	</tr>
																	<tr>
																		<td>1</td>
																		<td>
																			<span class="text-gray-800 fw-bolder text-end">Clock In 2</span>
																		</td>
																		<td>
																			<span class="text-gray-800 fw-bolder d-block fs-6">11:00 AM</span>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													</div>
											</div>
										</div>
										{{-- end absen karyawan gerai --}}
									</div>
									<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<div class="d-flex align-items-center">
														<span class="fs-2hx fw-bolder text-dark me-2 lh-1">75</span>
														<span class="badge badge-success fs-6 lh-1 py-1 px-2 d-flex flex-center" style="height: 22px">Transaksi</span>
													</div>
													<span class="text-gray-400 pt-1 fw-bold fs-6">(Daily)Jumlah Transaksi VS Jumlah Customer Terinput</span>
												</div>
											</div>
											<div class="card-body pt-2 pb-4 d-flex align-items-center">
												<div class="d-flex flex-center me-5 pt-2">
													<canvas id="jumlah_pelanggan_dan_input_chart" style="min-width: 25px; min-height: 25px"></canvas>
												</div>
											</div>
										</div>
										<div class="card card-flush h-md-50 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<span class="fs-2hx fw-bolder text-dark me-2 lh-1">Menu Terlaris</span>
													<span class="text-gray-400 pt-1 fw-bold fs-6">5 Menu Terlaris</span>
												</div>
											</div>
											<div class="card-body">
													<!--begin::Item-->
													<div class="d-flex flex-stack">
														<div class="d-flex align-items-center me-3">
															<img src="{{('berkas')}}/menu/menu1.jpg" class="me-3 w-30px" alt="" />
															<div class="flex-grow-1">
																<a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">Es Teh</a>
																<span class="text-gray-400 fw-bold d-block fs-6">Terjual 65% dari menu lainnya</span>
															</div>
														</div>
														<div class="d-flex align-items-center w-100 mw-125px">
															<div class="progress h-6px w-100 me-2 bg-light-success">
																<div class="progress-bar bg-success" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
															</div>
															<span class="text-gray-400 fw-bold">65%</span>
														</div>
													</div>

													<div class="separator separator-dashed my-4"></div>

													<!--begin::Item-->
													<div class="d-flex flex-stack">
														<div class="d-flex align-items-center me-3">
															<img src="{{('berkas')}}/menu/menu2.jpg" class="me-3 w-30px" alt="" />
															<div class="flex-grow-1">
																<a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">Es Kunyit</a>
																<span class="text-gray-400 fw-bold d-block fs-6">Terjual 70% dari menu lainnya</span>
															</div>
														</div>
														<div class="d-flex align-items-center w-100 mw-125px">
															<div class="progress h-6px w-100 me-2 bg-light-success">
																<div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
															</div>
															<span class="text-gray-400 fw-bold">70%</span>
														</div>
													</div>

													<div class="separator separator-dashed my-4"></div>

												</div>
										</div>
									</div>

									<div class="col-lg-12 col-xl-12 col-xxl-6 mb-5 mb-xl-0">
										<div class="card card-flush overflow-hidden h-md-100">
												<div class="card-header pt-7">
													<h3 class="card-title align-items-start flex-column">
														<span class="card-label fw-bolder text-dark">Transaksi Hari ini</span>
														<span class="text-gray-400 mt-1 fw-bold fs-6">pendapatan Rp.1,250,000</span>
													</h3>
													<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
														<span class="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
																<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															</svg>
														</span>
													</button>
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
														<div class="menu-item px-3">
															<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
														</div>
														<div class="separator mb-3 opacity-75"></div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Tarik Semua Transaksi (Excel)</a>
														</div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Tarik Transaksi Hari Ini (Excel)</a>
														</div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Tarik Transaksi Bulan ini (Excel)</a>
														</div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Setujui Jurnal (checked)</a>
														</div>
													</div>
												</div>
												<div class="card-body py-3">
									<!--begin::Table container-->
									<div class="table-responsive">
										<!--begin::Table-->
										<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
											<thead>
												<tr class="fw-bolder text-muted">
													<th class="w-25px">
														<div class="form-check form-check-sm form-check-custom form-check-solid">
															<input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" />
														</div>
													</th>
													<th class="min-w-150px">Order Id</th>
													<th class="min-w-140px">Karyawan</th>
													<th class="min-w-120px">Date</th>
													<th class="min-w-120px">Pembayaran</th>
													<th class="min-w-120px">Total</th>
													<th class="min-w-120px">Status</th>
													<th class="min-w-100px text-end">Actions</th>
												</tr>
											</thead>
											<!--end::Table head-->
											<!--begin::Table body-->
											<tbody>
												@for ($i=0; $i < 10; $i++)
													<tr>
														<td>
															<div class="form-check form-check-sm form-check-custom form-check-solid">
																<input class="form-check-input widget-13-check" type="checkbox" value="1" />
															</div>
														</td>
														<td>
															<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">5603{{$i}}-XDER</a>
														</td>
														<td>
															<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Tukiyem</span>
														</td>
														<td>
															<a href="#" class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">05/28/2020</a>
															<span class="text-muted fw-bold text-muted d-block fs-7">Tipe: Cash In</span>
														</td>
														<td>
															<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Cash</span>
															<span class="text-muted fw-bold text-muted d-block fs-7">Total Bayar: Rp.56,000</span>
														</td>
														<td class="text-dark fw-bolder text-hover-primary fs-6">Rp.56,000</td>
														<td>
															<span class="badge badge-light-danger">Invalid</span>
														</td>
														<td class="text-end">
															<button  class="showDetailRow btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
																<i class="fas fa-angle-double-down"></i>
															</button>
														</td>
													</tr>
													{{-- child row --}}

													{{-- end child row --}}
												@endfor
											</tbody>
										</table>
									</div>
								</div>
										</div>
									</div>

									<div class="col-lg-12 col-xl-12 col-xxl-6 mb-5 mb-xl-0">
										<div class="card card-flush overflow-hidden h-md-100">
												<div class="card-header pt-7">
													<h3 class="card-title align-items-start flex-column">
														<span class="card-label fw-bolder text-dark">List Karyawan</span>
														<span class="text-gray-400 mt-1 fw-bold fs-6">karyawan yang bekerja di gerai</span>
													</h3>

													<div class="card-toolbar" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk menambahkan Karyawan">
														<a href="#modalTambahKaryawan" class="btn btn-sm btn-light btn-active-primary" data-bs-toggle="modal">
															<i class="fas fa-plus"></i>
														Tambah Karyawan</a>
													</div>

												</div>
												<div class="card-body py-3">
									<!--begin::Table container-->
									<div class="table-responsive">
										<!--begin::Table-->
										<table class="table table-row-bordered gy-5 datatable_general">
											<thead>
												<tr class="fw-bolder text-muted">
													<th class="min-w-150px">Nama Karyawan</th>
													<th class="min-w-120px">Penjualan</th>
													<th class="min-w-100px text-end">Actions</th>
												</tr>
											</thead>
											<!--end::Table head-->
											<!--begin::Table body-->
											<tbody>
												<tr>
													<td>
														<div class="d-flex align-items-center">
															<div class="symbol symbol-45px me-5">
																<img src="{{('berkas')}}/karyawan/tukiyem.jpg" alt="" />
															</div>
															<div class="d-flex justify-content-start flex-column">
																<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">Tukiyem</a>
																<span class="text-muted fw-bold text-muted d-block fs-7">Kasir</span>
															</div>
														</div>
													</td>
													<td class="text-end">
														<div class="d-flex flex-column w-100 me-2">
															<div class="d-flex flex-stack mb-2">
																<span class="me-2 fs-7 fw-bold">Rp.560,000</span>
															</div>
														</div>
													</td>
													<td>
														<div class="d-flex justify-content-end flex-shrink-0">
															<a href="#modalUbahDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk mengubah data">
																<i class="fas fa-user-edit"></i>
															</a>
															<a href="#modalHapusDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk menghapus karyawan">
																<i class="fas fa-trash"></i>
															</a>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<div class="d-flex align-items-center">
															<div class="symbol symbol-45px me-5">
																<img src="{{('berkas')}}/karyawan/suketi.jpg" alt="" />
															</div>
															<div class="d-flex justify-content-start flex-column">
																<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">Suketi</a>
																<span class="text-muted fw-bold text-muted d-block fs-7">Kasir</span>
															</div>
														</div>
													</td>
													<td class="text-end">
														<div class="d-flex flex-column w-100 me-2">
															<div class="d-flex flex-stack mb-2">
																<span class="me-2 fs-7 fw-bold">Rp.320,000</span>
															</div>
														</div>
													</td>
													<td>
														<div class="d-flex justify-content-end flex-shrink-0">
															<a href="#modalUbahDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk mengubah data">
																<i class="fas fa-user-edit"></i>
															</a>
															<a href="#modalHapusDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk menghapus karyawan">
																<i class="fas fa-trash"></i>
															</a>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									</div>
										</div>
									</div>

							<div class="row gy-5 g-xl-10">
								<div class="col-xl-4">
									<div class="card card-flush h-xl-100">
										<div class="card-header pt-7 mb-3">
											<h3 class="card-title align-items-start flex-column">
												<span class="card-label fw-bolder text-gray-800">Stock Terpakai</span>
												<span class="text-gray-400 mt-1 fw-bold fs-6">4 stock paling sedikit Minggu ini</span>
											</h3>
										</div>
										<div class="card-body pt-4">

											<div class="d-flex flex-stack">
												<div class="d-flex align-items-center me-5">
													<div class="me-5">
														<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">Gula</a>
														<span class="text-gray-400 fw-bold fs-7 d-block text-start ps-0">234Kg Terpakai</span>
													</div>
												</div>
												<div class="text-gray-400 fw-bolder fs-7 text-end">
												<span class="text-gray-800 fw-bolder fs-6 d-block">25Kg</span>
												</div>
											</div>

											<div class="separator separator-dashed my-5"></div>

											<div class="text-center pt-9">
												<a href="../../demo18/dist/apps/ecommerce/catalog/add-product.html" class="btn btn-primary">Tambah Stok</a>
											</div>
										</div>
									</div>
								</div>


								<div class="col-xl-8">
									<div class="card card-flush h-xl-100">
										<div class="card-header pt-7">
											<h3 class="card-title align-items-start flex-column">
												<span class="card-label fw-bolder text-dark">Stock Report</span>
												<span class="text-gray-400 mt-1 fw-bold fs-6">Total 2,356 Items in the Stock</span>
											</h3>
										</div>
										<div class="card-body">
											<table class="table align-middle table-row-dashed fs-6 gy-3 datatable_general">
												<thead>
													<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
														<th class="min-w-100px">Item</th>
														<th class="text-end pe-3 min-w-100px">Product ID</th>
														<th class="text-end pe-3 min-w-150px">Date Added</th>
														<th class="text-end pe-3 min-w-100px">Price</th>
														<th class="text-end pe-3 min-w-50px">Status</th>
														<th class="text-end pe-0 min-w-25px">Qty</th>
													</tr>
												</thead>
												<tbody class="fw-bolder text-gray-600">
													<tr>
														<td>
															<a href="../../demo18/dist/apps/ecommerce/catalog/edit-product.html" class="text-dark text-hover-primary">Macbook Air M1</a>
														</td>
														<td class="text-end">#XGY-356</td>
														<td class="text-end">Jul 25, 2022</td>
														<td class="text-end">$1,230</td>
														<td class="text-end">
															<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
														</td>
														<td class="text-end" data-order="58">
															<span class="text-dark fw-bolder">58 PCS</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>


								</div>
							</div>
						</div>


@include('gerai.modal.modal-tambah-karyawan')
@include('gerai.modal.modal-ubah-data-karyawan')
@include('gerai.modal.modal-hapus-data-karyawan')

<script type="text/javascript">
	$(document).ready(function() {
		function format ( d ) {
			return 	'<table class="table table-row-bordered gy-5">'+
			'<tbody>'+
				'<tr>'+
					'<td></td>'+
					'<td>'+
						'<div class="d-flex align-items-center">'+
							'<div class="symbol symbol-50px me-5">'+
								'<img src="assets/media/stock/600x400/img-26.jpg" class="" alt="" />'+
							'</div>'+
							'<div class="d-flex justify-content-start flex-column">'+
								'<a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Bubble Tea</a>'+
								'<span class="text-muted fw-bold text-muted d-block fs-7">Rp.35,000</span>'+
								'<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">1x</span>'+

							'</div>'+
						'</div>'+
					'</td>'+
					'<td></td>'+
					'<td>'+
						'<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Total</span>'+
						'<span class="text-muted fw-bold text-muted d-block fs-7">Rp.35,000</span>'+
					'</td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
				'</tr>'+
				'<tr>'+
						'<td></td>'+
						'<td>'+
							'<div class="d-flex align-items-center">'+
								'<div class="symbol symbol-50px me-5">'+
									'<img src="assets/media/stock/600x400/img-18.jpg" class="" alt="" />'+
								'</div>'+
								'<div class="d-flex justify-content-start flex-column">'+
									'<a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Green Tea</a>'+
									'<span class="text-muted fw-bold text-muted d-block fs-7">Rp.25,000</span>'+
									'<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">1x</span>'+

								'</div>'+
							'</div>'+
						'</td>'+
						'<td></td>'+
						'<td>'+
							'<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Total</span>'+
							'<span class="text-muted fw-bold text-muted d-block fs-7">Rp.25,000</span>'+
						'</td>'+
						'<td></td>'+
						'<td></td>'+
						'<td></td>'+
						'<td></td>'+
					'</tr>'+
				'<tr>'+
					'<td></td>'+
					'<td>'+
						'<div class="d-flex align-items-center">'+
							'<div class="d-flex justify-content-start flex-column">'+
								'<a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Discount</a>'+
							'</div>'+
						'</div>'+
					'</td>'+
					'<td></td>'+
					'<td>'+
						'<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Total</span>'+
						'<span class="text-muted fw-bold text-muted d-block fs-7">Rp.4,000</span>'+
					'</td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
				'</tr>'+
				'</tbody>'+
				'</table>';
		}
		var table = $('#kt_datatable_example_1').DataTable( {
			"order": [[ 1, "asc" ]],
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
		// Add event listener for opening and closing details
    $('#kt_datatable_example_1 tbody').on('click', '.showDetailRow', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

		$('.datatable_general').DataTable( {
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
	});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script type="text/javascript">
var ctx = document.getElementById("penjualan_produk_hari_ini_chart");
        // tampilan chart
        var piechart = new Chart(ctx,{type: 'doughnut',
          data : {

        datasets: [{
          // Jumlah Value yang ditampilkan
           data:[50,4,46],

          backgroundColor:[
                 'rgba(0, 0, 255, 0.5)',
                 'rgba(255, 0, 0, 0.5)',
                 'rgba(0, 255, 0, 0.5)',
                 ]
        }],
        }
        });

var ctx = document.getElementById("jumlah_pelanggan_dan_input_chart");
        // tampilan chart
        var piechart = new Chart(ctx,{type: 'pie',
          data : {
						labels: [
				    'Transaksi',
				    'Berlangganan',
				  ],
        datasets: [{
          // Jumlah Value yang ditampilkan
           data:[70,5],

          backgroundColor:[
                 'rgba(0, 0, 255, 0.5)',
                 'rgba(255, 0, 0, 0.5)',
                 ]
        }],
        }
        });
</script>

@endsection
