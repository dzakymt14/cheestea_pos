<div class="modal fade pac-container" id="modal_tambah" tabindex="-1" aria-hidden="true">
<style>
    #map {
		height: 400px;
		/* The height is 400 pixels */
		width: 100%;
		/* The width is the width of the web page */
		}
		#description {
font-family: Roboto;
font-size: 15px;
font-weight: 300;
}

#infowindow-content .title {
font-weight: bold;
}

#infowindow-content {
display: none;
}

#map #infowindow-content {
display: inline;
}

.pac-card {
background-color: #fff;
border: 0;
border-radius: 2px;
box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
margin: 10px;
padding: 0 0.5em;
font: 400 18px Roboto, Arial, sans-serif;
overflow: hidden;
font-family: Roboto;
padding: 0;
}

#pac-container {
padding-bottom: 12px;
margin-right: 12px;
}

.pac-controls {
display: inline-block;
padding: 5px 11px;
}

.pac-controls label {
font-family: Roboto;
font-size: 13px;
font-weight: 300;
}

#pac-input {
background-color: #fff;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 400px;
}

#pac-input:focus {
border-color: #4d90fe;
}

#title {
color: #fff;
background-color: #4d90fe;
font-size: 25px;
font-weight: 500;
padding: 6px 12px;

}
</style>
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <form class="form" action="/gerai-post" method="POST" enctype="multipart/form-data" id="kt_tambah_data_form" autocomplete="off">
                        @csrf
                        <div class="modal-header" id="kt_tambah_data_header">
                            <h3 class="modal-title">Tambah Data</h3>
                            <div class="btn btn-sm btn-icon btn-active-warning btn-color-muted" data-bs-dismiss="modal">
                                <i class="fas fa-times"></i>
                            </div>
                        </div>
                        <div class="modal-body py-10 px-lg-17">
                            <!--begin::Scroll-->
                            <div class="scroll-y me-n7 pe-7" id="kt_tambah_data_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_tambah_data_header" data-kt-scroll-wrappers="#kt_tambah_data_scroll" data-kt-scroll-offset="300px">
                                <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                                    <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                                        </svg>
                                    </span>
                                    <div class="d-flex flex-stack flex-grow-1">
                                        <div class="fw-bold">
                                            <h5 class="text-gray-900 fw-bolder mb-0">Perhatian</h5>
                                            <div class="fs-6 text-gray-700">1. Gerai Anda Akan bersifat Aktif ketika ditambahkan</div>
                                            <div class="fs-6 text-gray-700">2. Karyawan dapat ditugaskan lewat fitur ini namun tidak diwajibkan</div>
                                            <div class="fs-6 text-gray-700">3. Karyawan yang diinput melalui fitur ini akan diberikan PIN yang berbeda secara otomatis</div>
                                            <div class="fs-6 text-gray-700">4. Master PIN adalah milik owner dan berbeda dengan PIN milik Karyawan</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-md-6 fv-row">
                                        <label class="required fs-5 fw-bold mb-2">Nama Gerai</label>
                                        <input type="text" class="form-control" placeholder="" name="nama_gerai" />
                                    </div>
                                </div>
                                <div class="row mb-5">
                                  <div class="col-md-6 fv-row mb-5">
                                      <label class="required fs-5 fw-bold mb-2">Master PIN</label>
                                      <input type="password" pattern="[0-9]{6}" class="form-control" placeholder="123xxx" maxlength="6" name="master_pin" id="txtNewPassword" autocomplete="new-password"/>

                                  </div>
                                  <div class="col-md-6 fv-row mb-5">
                                      <label class="required fs-5 fw-bold mb-2">Master Ulangi PIN</label>
                                      <input type="password" pattern="[0-9]{6}" class="form-control" placeholder="123xxx" maxlength="6" name="re_master_pin" id="txtConfirmPassword" />
                                      <div class="registrationFormAlert" style="color:green;" id="CheckPasswordMatch">
                                  </div>
                                </div>
                                <div class="row mb-5">
                                  <div class="col-12">
                                      <label class="required fs-5 fw-bold mb-2">Alamat</label>
                                      <textarea class="form-control" name="alamat" rows="5" cols="90"></textarea>
                                  </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-12">
                                        <label>Autocomplete search</label>
                                        <div style="display: none">
                                            <input
                                              type="radio"
                                              name="type"
                                              id="changetype-all"
                                              checked="checked"
                                            />
                                            <label for="changetype-all">All</label>
                                        </div>
                                        <div id="strict-bounds-selector" class="pac-controls" style="display: none">
                                            <input type="checkbox" id="use-location-bias" value="" checked />
                                            <label for="use-location-bias">Bias to map viewport</label>
                                        </div>
                                        <input class="form-control" id="alamatKu" type="text" placeholder="Enter a location" />
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-md-12">
                                        <div id="map" class="map-canvas" data-menara="lokasi" lok-lat="-1.2564674" lok-lng="116.874169" style="height: 300px"></div>
                                        <div id="infowindow-content">
                                            <span id="place-name" class="title"></span><br />
                                            <span id="place-address"></span>
                                        </div>
                                    </div>
                                </div>
                                <input class="lat" name="lat" hidden>
                                <input class="lng" name="lng" hidden>
                                <div class="mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label fw-bold">Karyawan</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="col-12">
                                      @php
                                          $listUser = App\User::where('level', 'karyawan')->get();
                                      @endphp
                                        <select class="form-select form-select-solid" name="id_user[]" data-kt-select2="true" multiple data-placeholder="Select option" data-allow-clear="true">
                                        <option></option>
                                        @foreach ($listUser as $listUsers)
                                            <option value="{{$listUsers->id}}">{{$listUsers->nama}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <!--end::Input-->
                                </div>
                             </div>
                        </div>
                        <div class="modal-footer flex-center">
                            <button type="reset" id="kt_tambah_data_cancel" class="btn btn-light me-3">Batal</button>
                            {{-- <button type="submit" id="kt_tambah_data_submit" class="btn btn-warning"> --}}
                            <button type="submit" id="simpan" class="btn btn-warning">
                                <span class="indicator-label text-dark">Tambah</span>
                                <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script async
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&libraries=places&callback=initMap">
    </script>
    <script>
    const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let labelIndex = 0;
    function initMap() {
                let posisi = { lat: -1.2564674, lng: 116.874169 };
                const map = new google.maps.Map(document.getElementById("map"), {
                    center: posisi,
                    zoom: 13,
                    // mapTypeControl: false,
                });
                google.maps.event.addListener(map, "click", (event) => {
                    let lat_user = event.latLng.lat();
                    let lng_user = event.latLng.lng();
                    addMarker(event.latLng, lat_user, lng_user, map);
                });

                const input = document.getElementById("alamatKu");
                const options = {
                    fields: ["formatted_address", "geometry", "name"],
                    strictBounds: true,
                    types: ["establishment"],
                };
                const autocomplete = new google.maps.places.Autocomplete(input, options);
                autocomplete.bindTo("bounds", map);
                const infowindow = new google.maps.InfoWindow();
                const infowindowContent = document.getElementById("infowindow-content");

                infowindow.setContent(infowindowContent);

                var marker = new google.maps.Marker({
                  draggable:true,
                    // position: posisi,
                    map: map,
                    animation : google.maps.Animation.BOUNCE,
                });
                google.maps.event.addListener(marker, 'dragend', function (evt) {
                    $('.lat').val(evt.latLng.lat().toFixed(5));
                    $('.lng').val(evt.latLng.lng().toFixed(5));
                });

                autocomplete.addListener("place_changed", () => {
                    infowindow.close();
                    marker.setVisible(false);

                    const place = autocomplete.getPlace();
                    let lat_user = place.geometry.location.lat();
                    let lng_user = place.geometry.location.lng();
                    $('.lat').val(lat_user.toFixed(5));
                    $('.lng').val(lng_user.toFixed(5));

                    if (!place.geometry || !place.geometry.location) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                    }

                    if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                    } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                    }

                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);
                    infowindowContent.children["place-name"].textContent = place.name;
                    infowindowContent.children["place-address"].textContent =
                    place.formatted_address;
                    infowindow.open(map, marker);
                });
    }
    </script>
