<div id="kt_header" class="header" data-kt-sticky="true" data-kt-sticky-name="header" data-kt-sticky-animation="false" data-kt-sticky-offset="{default: '200px', lg: '300px'}">
	<div class="container-fluid d-flex align-items-center flex-lg-stack">
		<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0 me-2 me-lg-5">
			<div class="flex-grow-1">
				<button class="btn btn-icon btn-color-warning btn-active-color-dark aside-toggle justify-content-start w-30px w-lg-40px" id="kt_aside_toggle">
					<span class="svg-icon svg-icon-2">
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" viewBox="0 0 16 15" fill="none">
							<rect y="6" width="16" height="3" rx="1.5" fill="black" />
							<rect opacity="0.3" y="12" width="8" height="3" rx="1.5" fill="black" />
							<rect opacity="0.3" width="12" height="3" rx="1.5" fill="black" />
						</svg>
					</span>
				</button>
				<a href="/dashboard">
					<img alt="Logo" src="{{asset('assets')}}/media/cheestea/icon.png" class="h-30px h-lg-35px" />
				</a>
			</div>
		</div>
		<div class="d-flex align-items-stretch flex-shrink-0">
			<a href="#" class="btn btn-light-primary" data-bs-toggle="modal" data-bs-target="#modal-absen"><span class="bullet bullet-dot bg-danger h-6px w-6px translate-middle animation-blink"></span><i class="fas fa-clock me-2"></i>Clock In</a>
			<div class="d-flex align-items-center ms-1 ms-lg-3">
				<div class="btn  btn-light-warning btn-icon btn-active-light-dark w-30px h-30px w-md-40px h-md-40px position-relative btn  btn-icon btn-active-light-dark w-30px h-30px w-md-40px h-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
					<span class="svg-icon svg-icon-2x">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
							<path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
							<rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
						</svg>
					</span>
				</div>
				<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-warning fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
					<div class="menu-item px-3">
						<div class="menu-content d-flex align-items-center px-3">
							<div class="symbol symbol-50px me-5">
								<img alt="Logo" src="{{asset('assets')}}/media/avatars/300-1.jpg" />
							</div>
							<div class="d-flex flex-column">
								<div class="fw-bolder d-flex align-items-center fs-5">Max Smith</div>
								<a href="#" class="fw-bold text-muted text-hover-warning fs-7">max@kt.com</a>
							</div>
						</div>
					</div>
					<div class="separator my-2"></div>
					<div class="menu-item px-5">
						<a href="#!" class="menu-link px-5"><i class="fas fa-user me-2 text-muted"></i>My Profile</a>
					</div>
					<div class="menu-item px-5">
						<a href="#!" class="menu-link px-5">
							<span class="menu-text"><i class="fas fa-bell me-2 text-muted"></i>My Projects</span>
							<span class="menu-badge">
								<span class="badge badge-light-danger badge-circle fw-bolder fs-7">3</span>
							</span>
						</a>
					</div>
					<div class="separator my-2"></div>
					<div class="menu-item px-5">
						<a class="menu-link px-5" href="{{ route('logout') }}"
						onclick="event.preventDefault();
						document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt text-danger me-2"></i> Sign Out
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@include('page.add.modal-absen')
