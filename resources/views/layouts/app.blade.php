<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="description" content="Deskripsi">
	<meta name="keywords" content="Keyword">

	<title>Cheestea Online</title>
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
	<meta property="og:url" content="https://keenthemes.com/metronic" />
	<meta property="og:site_name" content="Keenthemes | Metronic" />
	<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
	<link rel="shortcut icon" href="{{asset('assets')}}/media/cheestea/icon.png" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<link href="{{asset('assets')}}/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets')}}/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets')}}/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets')}}/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled">

	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="page d-flex flex-row flex-column-fluid">
			<!--begin::Wrapper-->
			<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
				@yield('sidebar')
				@yield('header')
				@yield('content')
				@yield('footer')
			</div>
			<!--end::Wrapper-->
		</div>
		<!--end::Page-->
	</div>

	<script>var hostUrl = "{{asset('assets')}}/";</script>
	<script src="{{asset('assets')}}/plugins/global/plugins.bundle.js"></script>
	<script src="{{asset('assets')}}/js/scripts.bundle.js"></script>
	<script src="{{asset('assets')}}/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
	<script src="{{asset('assets')}}/plugins/custom/datatables/datatables.bundle.js"></script>
	<script src="{{asset('assets')}}/js/widgets.bundle.js"></script>
	<script src="{{asset('assets')}}/js/custom/widgets.js"></script>
	<script src="{{asset('assets')}}/js/custom/apps/chat/chat.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-project/type.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-project/budget.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-project/settings.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-project/team.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-project/targets.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-project/files.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-project/complete.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-project/main.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/create-app.js"></script>
	<script src="{{asset('assets')}}/js/custom/utilities/modals/users-search.js"></script>
	{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&sensor=false"></script> --}}
</body>
</html>
