<div id="kt_aside" class="aside px-5" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '285px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_toggle">
	<div class="aside-menu flex-column-fluid">
		<div class="hover-scroll-overlay-y my-5 me-n4 pe-4" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_footer" data-kt-scroll-wrappers="#kt_aside, #kt_aside_menu" data-kt-scroll-offset="2px">
			<div class="menu menu-column menu-active-bg menu-state-primary menu-title-gray-700 fs-6 menu-rounded w-100 fw-bold" id="#kt_aside_menu" data-kt-menu="true">


				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">

						<span class="menu-title">Q.Milk Cheestea</span>
					</a>
				</div>

				<div class="menu-item">
					<div class="menu-content">
						<div class="separator mx-1 my-4"></div>
					</div>
				</div>

				<div class="menu-item">
					<div class="menu-content pt-8 pb-0">
						<span class="menu-section text-muted text-uppercase fs-8 ls-1">Main Menu</span>
					</div>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-store"></i>
						</span>
						<span class="menu-title">Gerai</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Karyawan</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Menu</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Inventory</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Customer</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Absensi</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Laporan</span>
					</a>
				</div>

				<div class="menu-item">
					<div class="menu-content">
						<div class="separator mx-1 my-4"></div>
					</div>
				</div>

				<div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
								</svg>
							</span>
						</span>
						<span class="menu-title">Owner Function</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg">
						<div class="menu-item">
							<a class="menu-link active" href="/gerai-index">
								<span class="menu-bullet">
									<i class="fas fa-home"></i>
								</span>
								<span class="menu-title">Gerai</span>
							</a>
						</div>
						<div class="menu-item">
							<a class="menu-link" href="/gerai-index">
								<span class="menu-bullet">
									<i class="fas fa-book"></i>
								</span>
								<span class="menu-title">Menu</span>
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
