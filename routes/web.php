<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');
// require __DIR__.'/auth.php';


Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/index', function () {
    return view('page.index');
});

Route::get('/pin', function () {
    return view('page.pin');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//karyawan
Route::get('/karyawan-index',['as' => 'KaryawanController.karyawanIndex', 'uses' => 'KaryawanController@karyawanIndex']);
Route::post('/karyawan-post',['as' => 'KaryawanController.karyawanPost', 'uses' => 'KaryawanController@karyawanPost']);

//gerai
Route::get('/gerai-index/{status}',['as' => 'GeraiController.geraiIndex', 'uses' => 'GeraiController@geraiIndex']);
Route::post('/gerai-post',['as' => 'GeraiController.geraiPost', 'uses' => 'GeraiController@geraiPost']);
Route::post('/gerai-aktifasi/{code}',['as' => 'GeraiController.geraiAktifasi', 'uses' => 'GeraiController@geraiAktifasi']);
Route::get('/gerai-detail',['as' => 'GeraiController.geraiDetail', 'uses' => 'GeraiController@geraiDetail']);


//menu
Route::get('/menu-index',['as' => 'MenuController.index', 'uses' => 'MenuController@index']);
Route::post('/menu-post',['as' => 'MenuController.menuPost', 'uses' => 'MenuController@menuPost']);

//kasir
Route::get('/kasir',['as' => 'KasirController.kasir', 'uses' => 'KasirController@kasir']);
Route::post('/kasir-post',['as' => 'KasirController.kasirPost', 'uses' => 'KasirController@kasirPost']);

//inventory
Route::get('/inventory-index',['as' => 'InventoryController.inventoryIndex', 'uses' => 'InventoryController@inventoryIndex']);
Route::post('/inventory-post',['as' => 'InventoryController.inventoryPost', 'uses' => 'InventoryController@inventoryPost']);
